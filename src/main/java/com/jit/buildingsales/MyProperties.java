package com.jit.buildingsales;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ziningmei
 * 接收application.yml中的myProps下面的属性
 */
@Component
@ConfigurationProperties
public class MyProperties {
    private String imagePath;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
