package com.jit.buildingsales.model;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "advise")
public class AdviseDO {

    @Id
    private Integer id;

    private Integer userid;

    private String content;

    private Date date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
