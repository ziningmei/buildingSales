package com.jit.buildingsales.model;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "collection")
public class CollectionDO {

    @Id
    private Integer id;

    private Integer userid;

    private Integer resourceid;

    private Date date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getResourceid() {
        return resourceid;
    }

    public void setResourceid(Integer resourceid) {
        this.resourceid = resourceid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
