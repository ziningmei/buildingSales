package com.jit.buildingsales.model;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "houseresource")
public class HouseResourceDO {

    @Id
    private Integer id;
    private String name;
    private Double poix;
    private Double poiy;
    private String district;
    private String address;
    private Integer floor;
    private String description;
    private String area;
    private Integer price;
    private String image;
    private Integer status;

    private Integer userid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPoix() {
        return poix;
    }

    public void setPoix(Double poix) {
        this.poix = poix;
    }

    public Double getPoiy() {
        return poiy;
    }

    public void setPoiy(Double poiy) {
        this.poiy = poiy;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }


    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
