package com.jit.buildingsales;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ziningmei
 */
@SpringBootApplication
@MapperScan("com.jit.buildingsales.dao")
public class BuildingSalesApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildingSalesApplication.class, args);
    }

}
