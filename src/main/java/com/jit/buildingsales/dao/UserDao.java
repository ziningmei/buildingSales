package com.jit.buildingsales.dao;

import com.jit.buildingsales.global.BaseMapper;
import com.jit.buildingsales.model.UserDO;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends BaseMapper<UserDO> {



}
