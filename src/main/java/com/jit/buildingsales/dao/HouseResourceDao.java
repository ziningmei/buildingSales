package com.jit.buildingsales.dao;

import com.jit.buildingsales.global.BaseMapper;
import com.jit.buildingsales.model.HouseResourceDO;
import org.springframework.stereotype.Repository;

@Repository
public interface HouseResourceDao extends BaseMapper<HouseResourceDO> {



}
