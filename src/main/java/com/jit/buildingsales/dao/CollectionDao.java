package com.jit.buildingsales.dao;

import com.jit.buildingsales.global.BaseMapper;
import com.jit.buildingsales.model.CollectionDO;
import org.springframework.stereotype.Repository;

@Repository
public interface CollectionDao extends BaseMapper<CollectionDO> {



}
