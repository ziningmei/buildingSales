package com.jit.buildingsales.dao;

import com.jit.buildingsales.global.BaseMapper;
import com.jit.buildingsales.model.AdviseDO;
import com.jit.buildingsales.model.AppointmentDO;
import org.springframework.stereotype.Repository;

@Repository
public interface AdviseDao extends BaseMapper<AdviseDO> {



}
