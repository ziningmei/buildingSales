package com.jit.buildingsales.util;

import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionTool {
    public SessionTool() {
    }

    public static Object getSessionValue(HttpServletRequest request, String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        } else {
            HttpSession session = request.getSession();
            return session == null ? null : session.getAttribute(key);
        }
    }
}
