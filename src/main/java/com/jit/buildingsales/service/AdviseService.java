package com.jit.buildingsales.service;

import com.jit.buildingsales.model.AdviseDO;
import com.jit.buildingsales.vo.AdviseVO;

import java.util.List;

public interface AdviseService {


    int insertAdvise(AdviseVO adviseVO);

    int deleteAdvise(AdviseVO adviseVO);

    List<AdviseDO> getAdviseList(AdviseVO adviseVO);
}
