package com.jit.buildingsales.service;

import com.jit.buildingsales.model.AppointmentDO;
import com.jit.buildingsales.vo.AppointmentVO;

import java.util.List;

public interface AppointmentService {
    int appointmentTime(AppointmentVO appointmentVO);

    int updateAppointment(AppointmentVO appointmentVO);

    List<AppointmentDO> getMyAppointment(AppointmentVO appointmentVO);
}
