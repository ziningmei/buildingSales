package com.jit.buildingsales.service;

import com.jit.buildingsales.model.HouseResourceDO;
import com.jit.buildingsales.vo.HouseResourceVO;

import java.util.List;

public interface HouseResourceService {


    int uploadResource(HouseResourceVO houseResourceVO);

    int deleteResource(HouseResourceVO houseResourceVO);

    List<HouseResourceDO> getMyResource(HouseResourceVO houseResourceVO);

    int updateResource(HouseResourceVO houseResourceVO);

    HouseResourceDO getResourceById(HouseResourceVO houseResourceVO);

    List<HouseResourceDO> getResourceList(HouseResourceVO houseResourceVO);

    List<HouseResourceDO> getResourceListByIds(HouseResourceVO houseResourceVO);
}
