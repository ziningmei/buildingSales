package com.jit.buildingsales.service;

import com.jit.buildingsales.model.UserDO;
import com.jit.buildingsales.vo.UserVO;

import java.util.List;

public interface UserService {


	/**
	 * 用户登陆
	 * @param userVO
	 */
	UserDO login(UserVO userVO);

	int register(UserVO userVO);

	int updateUser(UserVO user);

	UserDO getUserInfo(UserVO user);

    int deleteUserInfo(UserVO user);

    List<UserDO> getUserList(UserVO userVO);
}
