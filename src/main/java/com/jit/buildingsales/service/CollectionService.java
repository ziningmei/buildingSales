package com.jit.buildingsales.service;

import com.jit.buildingsales.model.CollectionDO;
import com.jit.buildingsales.vo.CollectionVO;

import java.util.List;

public interface CollectionService {


    List<CollectionDO> getMyCollection(CollectionVO collectionVO);

    int insertCollection(CollectionVO collectionVO);

    int deleteCollection(CollectionVO collectionVO);
}
