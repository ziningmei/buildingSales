package com.jit.buildingsales.service.impl;

import com.jit.buildingsales.dao.HouseResourceDao;
import com.jit.buildingsales.model.HouseResourceDO;
import com.jit.buildingsales.service.HouseResourceService;
import com.jit.buildingsales.vo.HouseResourceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

@Service
public class HouseResourceServiceImpl implements HouseResourceService {

    @Autowired
    HouseResourceDao houseResourceDao;

    @Override
    public int uploadResource(HouseResourceVO houseResourceVO) {
        return houseResourceDao.insert(houseResourceVO);
    }

    @Override
    public int deleteResource(HouseResourceVO houseResourceVO) {
        return houseResourceDao.deleteByPrimaryKey(houseResourceVO);
    }

    @Override
    public List<HouseResourceDO> getMyResource(HouseResourceVO houseResourceVO) {

        Example example = new Example(HouseResourceDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userid", houseResourceVO.getUserid());
        if(houseResourceVO.getStatus()>0){
            criteria.andEqualTo("status",houseResourceVO.getStatus());
        }

        List<HouseResourceDO> houseResourceDOS = houseResourceDao.selectByExample(example);

        return houseResourceDOS;
    }

    @Override
    public int updateResource(HouseResourceVO houseResourceVO) {
        return houseResourceDao.updateByPrimaryKeySelective(houseResourceVO);
    }

    @Override
    public HouseResourceDO getResourceById(HouseResourceVO houseResourceVO) {
        return houseResourceDao.selectByPrimaryKey(houseResourceVO);
    }

    @Override
    public List<HouseResourceDO> getResourceList(HouseResourceVO houseResourceVO) {
        Example example = new Example(HouseResourceDO.class);

        if(houseResourceVO.getStatus()>0){
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("status",houseResourceVO.getStatus());
        }
        if (StringUtil.isNotEmpty(houseResourceVO.getField())) {
            if (houseResourceVO.isAsc()) {
                example.orderBy(houseResourceVO.getField()).asc();
            } else {
                example.orderBy(houseResourceVO.getField()).desc();
            }
        }

        List<HouseResourceDO> houseResourceDOS = houseResourceDao.selectByExample(example);

        return houseResourceDOS;

    }

    @Override
    public List<HouseResourceDO> getResourceListByIds(HouseResourceVO houseResourceVO) {
        Example example = new Example(HouseResourceDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("id",houseResourceVO.getIds());
        if(houseResourceVO.getStatus()>0){
            criteria.andEqualTo("status",houseResourceVO.getStatus());
        }
        List<HouseResourceDO> houseResourceDOS = houseResourceDao.selectByExample(example);
        return houseResourceDOS;
    }
}
