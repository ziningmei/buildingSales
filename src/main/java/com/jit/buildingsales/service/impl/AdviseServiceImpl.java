package com.jit.buildingsales.service.impl;

import com.jit.buildingsales.dao.AdviseDao;
import com.jit.buildingsales.model.AdviseDO;
import com.jit.buildingsales.service.AdviseService;
import com.jit.buildingsales.vo.AdviseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class AdviseServiceImpl implements AdviseService {

    @Autowired
    private AdviseDao adviseDao;

    @Override
    public int insertAdvise(AdviseVO adviseVO) {
        return adviseDao.insert(adviseVO);
    }

    @Override
    public int deleteAdvise(AdviseVO adviseVO) {
        return adviseDao.deleteByPrimaryKey(adviseVO);
    }

    @Override
    public List<AdviseDO> getAdviseList(AdviseVO adviseVO) {
        
        Example example = new Example(AdviseDO.class);
        example.orderBy("date").desc();
        return adviseDao.selectByExample(example);
    }
}
