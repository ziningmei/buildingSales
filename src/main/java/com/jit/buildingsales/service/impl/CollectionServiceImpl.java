package com.jit.buildingsales.service.impl;

import com.jit.buildingsales.dao.CollectionDao;
import com.jit.buildingsales.model.CollectionDO;
import com.jit.buildingsales.service.CollectionService;
import com.jit.buildingsales.vo.CollectionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
class CollectionServiceImpl implements CollectionService {

    @Autowired
    CollectionDao collectionDao;


    @Override
    public List<CollectionDO> getMyCollection(CollectionVO collectionVO) {

        Example example = new Example(CollectionDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userid", collectionVO.getUserid());
        List<CollectionDO> collectionDOS = collectionDao.selectByExample(example);
        return collectionDOS;
    }

    @Override
    public int insertCollection(CollectionVO collectionVO) {
        return collectionDao.insert(collectionVO);
    }

    @Override
    public int deleteCollection(CollectionVO collectionVO) {
        return collectionDao.deleteByPrimaryKey(collectionVO);
    }
}
