package com.jit.buildingsales.service.impl;

import com.jit.buildingsales.dao.AppointmentDao;
import com.jit.buildingsales.model.AppointmentDO;
import com.jit.buildingsales.service.AppointmentService;
import com.jit.buildingsales.vo.AppointmentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    AppointmentDao appointmentDao;


    @Override
    public int appointmentTime(AppointmentVO appointmentVO) {

        return appointmentDao.insert(appointmentVO);
    }

    @Override
    public int updateAppointment(AppointmentVO appointmentVO) {

        return appointmentDao.updateByPrimaryKeySelective(appointmentVO);
    }

    @Override
    public List<AppointmentDO> getMyAppointment(AppointmentVO appointmentVO) {


        Example example = new Example(AppointmentDO.class);
        Example.Criteria criteria = example.createCriteria();
        if(appointmentVO.getAdmin()>0){
            criteria.andEqualTo("admin", appointmentVO.getAdmin());
        }else if (appointmentVO.getConsumer()>0){
            criteria.andEqualTo("consumer", appointmentVO.getConsumer());
        }

        List<AppointmentDO> appointmentDOS = appointmentDao.selectByExample(example);
        return appointmentDOS;
    }


}
