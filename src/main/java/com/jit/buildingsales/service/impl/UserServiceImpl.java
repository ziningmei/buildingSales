package com.jit.buildingsales.service.impl;

import com.jit.buildingsales.dao.UserDao;
import com.jit.buildingsales.model.UserDO;
import com.jit.buildingsales.service.UserService;
import com.jit.buildingsales.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public UserDO login(UserVO userVO) {

        Example example = new Example(UserDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("username", userVO.getUsername());
        criteria.andEqualTo("password", userVO.getPassword());
        criteria.andEqualTo("role",userVO.getRole());
        List<UserDO> userDOS = userDao.selectByExample(example);

        if(userDOS.size()>0){
            return userDOS.get(0);
        }else {
            return null;
        }


    }

    @Override
    public int register(UserVO userVO) {

        return userDao.insert(userVO);
    }

    @Override
    public int updateUser(UserVO userVO) {
        return userDao.updateByPrimaryKey(userVO);
    }

    @Override
    public UserDO getUserInfo(UserVO user) {
        return userDao.selectByPrimaryKey(user);
    }

    @Override
    public int deleteUserInfo(UserVO user) {
        return userDao.deleteByPrimaryKey(user);
    }

    @Override
    public List<UserDO> getUserList(UserVO userVO) {
        return userDao.selectAll();
    }

}
