package com.jit.buildingsales.vo;

import com.jit.buildingsales.model.HouseResourceDO;

import java.util.List;

public class HouseResourceVO extends HouseResourceDO {

    /**
     * 排序字段名
     */
    private String field;

    /**
     * 顺序，true，正序，false，倒序
     */
    private boolean isAsc;

    private List<Integer> ids;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public boolean isAsc() {
        return isAsc;
    }

    public void setAsc(boolean asc) {
        isAsc = asc;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }
}
