package com.jit.buildingsales.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "返回结果")
public class Result<T> {

    @ApiModelProperty(value = "是否成功", required = true)
    private boolean success = true;

    @ApiModelProperty(value = "错误码", required = true)
    private String errorMsg;

    @ApiModelProperty(value = "处理成功后，返回的数据", required = true)
    private T model;

    public Result withModel(T model) {
        this.model = model;
        return this;
    }

    public  Result withError(String error) {

        this.success = false;
        this.errorMsg = error;

        return this;

    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }
}
