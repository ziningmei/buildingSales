package com.jit.buildingsales.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author ziningmei
 */
@Controller
public class BaseController {

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView index() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("login");
    }
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView register() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("register");
    }
    @RequestMapping(value = "/sellList", method = RequestMethod.GET)
    public ModelAndView sellList() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("sellList");
    }
    @RequestMapping(value = "/sellDetail", method = RequestMethod.GET)
    public ModelAndView sellDetail() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("sellDetail");
    }
    @RequestMapping(value = "/loginUser", method = RequestMethod.GET)
    public ModelAndView loginUser() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("loginUser");
    }
    @RequestMapping(value = "/fav", method = RequestMethod.GET)
    public ModelAndView fav() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("fav");
    }
    @RequestMapping(value = "/my", method = RequestMethod.GET)
    public ModelAndView my() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("my");
    }
    @RequestMapping(value = "/ordersShow", method = RequestMethod.GET)
    public ModelAndView ordersShow() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("ordersShow");
    }
    @RequestMapping(value = "/roomsAdd", method = RequestMethod.GET)
    public ModelAndView  roomsAdd() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("roomsAdd");
    }
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public ModelAndView  contact() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("contact");
    }
    @RequestMapping(value = "/ordersAdd", method = RequestMethod.GET)
    public ModelAndView  ordersAdd() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("ordersAdd");
    }
    @RequestMapping(value = "/map", method = RequestMethod.GET)
    public ModelAndView  map() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("map");
    }


    @RequestMapping(value = "/back/login", method = RequestMethod.GET)
    public ModelAndView backLogin() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/login");
    }
    @RequestMapping(value = "/back/main", method = RequestMethod.GET)
    public ModelAndView backMain() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/main");
    }

    @RequestMapping(value = "/back/top", method = RequestMethod.GET)
    public ModelAndView backTop() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/top");
    }
    @RequestMapping(value = "/back/left", method = RequestMethod.GET)
    public ModelAndView backLeft() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/left");
    }
    @RequestMapping(value = "/back/showUser", method = RequestMethod.GET)
    public ModelAndView backShowUser() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/showUser");
    }
    @RequestMapping(value = "/back/add", method = RequestMethod.GET)
    public ModelAndView backAdd() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/add");
    }
    @RequestMapping(value = "/back/showFeedback", method = RequestMethod.GET)
    public ModelAndView backShowFeedBack() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/showFeedback");
    }
    @RequestMapping(value = "/back/showHouses", method = RequestMethod.GET)
    public ModelAndView backShowHouses() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/showHouses");
    }
    @RequestMapping(value = "/back/updateUser", method = RequestMethod.GET)
    public ModelAndView backUpdateUser() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/updateUser");
    }
    @RequestMapping(value = "/back/updateHouses", method = RequestMethod.GET)
    public ModelAndView backUpdateHouses() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/updateHouses");
    }
    @RequestMapping(value = "/back/updateFeedback", method = RequestMethod.GET)
    public ModelAndView backUpdateFeedBack() {
        // 页面位置 /WEB-INF/jsp/page/page.jsp
        return new ModelAndView("back/updateFeedback");
    }



}
