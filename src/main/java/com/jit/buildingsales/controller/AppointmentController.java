package com.jit.buildingsales.controller;

import com.jit.buildingsales.model.AppointmentDO;
import com.jit.buildingsales.model.CollectionDO;
import com.jit.buildingsales.service.AppointmentService;
import com.jit.buildingsales.vo.AppointmentVO;
import com.jit.buildingsales.vo.Result;
import com.jit.buildingsales.vo.ResultList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Api(description = "房源收藏信息")
@RestController
@RequestMapping("/appointment")
public class AppointmentController extends BasicController {

    @Autowired
    AppointmentService appointmentService;


    @ApiOperation(value = "预约看房", notes = "预约看房")
    @RequestMapping(value = "/appointmentTime", method = RequestMethod.POST)
    public Result appointmentTime(@RequestBody AppointmentVO appointmentVO) {


        if (appointmentVO.getConsumer() <= 0 || appointmentVO.getAdmin() <= 0 || appointmentVO.getResourceid() <= 0) {
            return new Result().withError("输入不正确");
        }

        try {

            int rows = appointmentService.appointmentTime(appointmentVO);

            if (rows <= 0) {
                return new Result<>().withError("预约失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }
        return new Result<>();

    }

    @ApiOperation(value = "通过预约", notes = "通过预约")
    @RequestMapping(value = "/updateAppointment", method = RequestMethod.POST)
    public Result<CollectionDO> updateAppointment(@RequestBody AppointmentVO appointmentVO) {


        if (appointmentVO.getId() <= 0) {
            return new Result().withError("输入不正确");
        }

        try {

            int rows = appointmentService.updateAppointment(appointmentVO);

            if (rows <= 0) {
                return new Result<>().withError("通过预约失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }
        return new Result<>();

    }

    @ApiOperation(value = "获取我的预约", notes = "获取我的预约")
    @RequestMapping(value = "/getMyAppointment", method = RequestMethod.POST)
    public ResultList<AppointmentDO> getMyAppointment(@RequestBody AppointmentVO appointmentVO) {


        if (appointmentVO.getAdmin() <= 0 && appointmentVO.getConsumer() <= 0) {
            return new ResultList().withError("输入不正确");
        }

        try {

            List<AppointmentDO> rows = appointmentService.getMyAppointment(appointmentVO);

            return new ResultList<AppointmentDO>().withModel(rows);


        } catch (Exception e) {
            e.printStackTrace();
            return new ResultList().withError("系统错误");
        }

    }


}
