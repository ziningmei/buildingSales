package com.jit.buildingsales.controller;

import com.jit.buildingsales.model.CollectionDO;
import com.jit.buildingsales.service.CollectionService;
import com.jit.buildingsales.vo.CollectionVO;
import com.jit.buildingsales.vo.Result;
import com.jit.buildingsales.vo.ResultList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;


@Api(description = "房源收藏信息")
@RestController
@RequestMapping("/collection")
public class CollectionController extends BasicController {

    @Autowired
    CollectionService collectionService;


    @ApiOperation(value = "查看我的收藏", notes = "查看我的收藏")
    @RequestMapping(value = "/getMyCollection", method = RequestMethod.POST)
    public ResultList<CollectionDO> getMyCollection(@RequestBody CollectionVO collectionVO, HttpSession session) {


        if (collectionVO.getUserid() <= 0) {
            return new ResultList().withError("输入不正确");
        }

        try {

            List<CollectionDO> rows = collectionService.getMyCollection(collectionVO);
            session.setAttribute("collectionlist",rows);
            return new ResultList<CollectionDO>().withModel(rows);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResultList().withError("系统错误");
        }

    }

    @ApiOperation(value = "收藏房源", notes = "收藏房源")
    @RequestMapping(value = "/insertCollection", method = RequestMethod.POST)
    public Result<Boolean> insertCollection(@RequestBody CollectionVO collectionVO) {


        if (collectionVO.getUserid() <= 0 || collectionVO.getResourceid() <= 0) {
            return new Result().withError("输入不正确");
        }

        try {
               // httpRequest.getSession().setAttribute();
              // collectionVO.setUserid(user.getUid);
            int rows = collectionService.insertCollection(collectionVO);

            if (rows <= 0) {
                return new Result<>().withError("收藏失败");
            }


        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }
        return new Result<>().withModel(true);

    }

    @ApiOperation(value = "删除收藏", notes = "删除收藏")
    @RequestMapping(value = "/deleteCollection", method = RequestMethod.POST)
    public Result<Boolean> deleteCollection(@RequestBody CollectionVO collectionVO) {


        if (collectionVO.getId() <= 0) {
            return new Result().withError("输入不正确");
        }

        try {

            int rows = collectionService.deleteCollection(collectionVO);

            if (rows <= 0) {
                return new Result<>().withError("取消收藏失败");
            }


        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }
        return new Result<>().withModel(true);

    }




}
