package com.jit.buildingsales.controller;

import com.jit.buildingsales.MyProperties;
import com.jit.buildingsales.model.HouseResourceDO;
import com.jit.buildingsales.service.HouseResourceService;
import com.jit.buildingsales.vo.HouseResourceVO;
import com.jit.buildingsales.vo.Result;
import com.jit.buildingsales.vo.ResultList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.util.StringUtil;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.List;

@Api(description = "房源信息")
@RestController
@RequestMapping("/houseResource")
public class HouseResourceController extends BasicController {

    @Autowired
    HouseResourceService houseResourceService;

    @Autowired
    MyProperties myProperties;

    @ApiOperation(value = "上传图片", notes = "上传图片")
    @RequestMapping(value = "/fileUpload", headers = "content-type=multipart/form-data", method = RequestMethod.POST)
    public Result<String> fileUpload(@RequestParam("file") @ApiParam(value = "csv文件", required = true) MultipartFile file) {
        //前置校验

        if (file.isEmpty()) {
            return new Result<>().withError("上传失败，请选择文件");
        }

        String fileName = file.getOriginalFilename();

        File dest = new File(myProperties.getImagePath() + fileName);
        try {
            file.transferTo(dest);

            return new Result<>().withModel(fileName);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }


    }

    @ApiOperation(value = "上传房源信息", notes = "上传房源信息")
    @RequestMapping(value = "/uploadResource", method = RequestMethod.POST)
    public Result uploadResource(@RequestBody HouseResourceVO houseResourceVO) {


        if (StringUtils.isEmpty(houseResourceVO.getName()) || StringUtil.isEmpty(houseResourceVO.getAddress())) {
            return new Result().withError("输入不正确");
        }

        try {

            int rows = houseResourceService.uploadResource(houseResourceVO);
            if (rows <= 0) {
                return new Result().withModel(true);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

        return new Result();
    }

    @ApiOperation(value = "修改房源信息", notes = "修改房源信息")
    @RequestMapping(value = "/updateResource", method = RequestMethod.POST)
    public Result updateResource(@RequestBody HouseResourceVO houseResourceVO) {


        if (houseResourceVO.getId() <= 0) {
            return new Result().withError("输入不正确");
        }

        try {

            int rows = houseResourceService.updateResource(houseResourceVO);
            if (rows <= 0) {
                return new Result().withModel(true);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

        return new Result();
    }

    @ApiOperation(value = "删除房源信息", notes = "删除房源信息")
    @RequestMapping(value = "/deleteResource", method = RequestMethod.POST)
    public Result<Boolean> deleteResource(@RequestBody HouseResourceVO houseResourceVO) {


        if (houseResourceVO.getId() <= 0) {
            return new Result().withError("输入不正确");
        }

        try {

            int rows = houseResourceService.deleteResource(houseResourceVO);
            if (rows <= 0) {
                return new Result().withModel(true);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

        return new Result();
    }

    @ApiOperation(value = "查看我的房源信息", notes = "查看我的房源信息")
    @RequestMapping(value = "/getMyResource", method = RequestMethod.POST)
    public ResultList<HouseResourceDO> getMyResource(@RequestBody HouseResourceVO houseResourceVO) {


        if (houseResourceVO.getUserid() <= 0) {
            return new ResultList().withError("输入不正确");
        }

        try {

            List<HouseResourceDO> rows = houseResourceService.getMyResource(houseResourceVO);

            return new ResultList<HouseResourceDO>().withModel(rows);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResultList().withError("系统错误");
        }

    }

    @ApiOperation(value = "查看房源信息", notes = "查看房源信息")
    @RequestMapping(value = "/getResourceById", method = RequestMethod.POST)
    public Result<HouseResourceDO> getResourceById(@RequestBody HouseResourceVO houseResourceVO) {


        if (houseResourceVO.getId() <= 0) {
            return new Result().withError("输入不正确");
        }

        try {

            HouseResourceDO result = houseResourceService.getResourceById(houseResourceVO);

            return new Result().withModel(result);


        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

    }


    @ApiOperation(value = "查看房源信息列表", notes = "查看房源信息列表")
    @RequestMapping(value = "/getResourceList", method = RequestMethod.POST)
    public ResultList<HouseResourceDO> getResourceList(@RequestBody HouseResourceVO houseResourceVO) {

        try {

            List<HouseResourceDO> rows = houseResourceService.getResourceList(houseResourceVO);
           // session.setAttribute("houselist",rows);
            return new ResultList<HouseResourceDO>().withModel(rows);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResultList().withError("系统错误");
        }

    }

    @ApiOperation(value = "根据id查看房源信息列表", notes = "根据id查看房源信息列表")
    @RequestMapping(value = "/getResourceListByIds", method = RequestMethod.POST)
    public ResultList<HouseResourceDO> getResourceListByIds(@RequestBody HouseResourceVO houseResourceVO) {

        try {

            List<HouseResourceDO> rows = houseResourceService.getResourceListByIds(houseResourceVO);
            //session.setAttribute("houselistbyids",rows);
            return new ResultList<HouseResourceDO>().withModel(rows);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResultList().withError("系统错误");
        }

    }
}
