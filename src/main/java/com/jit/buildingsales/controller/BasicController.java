package com.jit.buildingsales.controller;

import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class BasicController {
    HttpServletResponse httpResponse;
    HttpServletRequest httpRequest;
    HttpSession httpSession;

    @ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response) {
        httpRequest = request;
        httpResponse = response;
        httpSession = request.getSession();
    }

}
