package com.jit.buildingsales.controller;

import com.jit.buildingsales.model.AdviseDO;
import com.jit.buildingsales.service.AdviseService;
import com.jit.buildingsales.vo.AdviseVO;
import com.jit.buildingsales.vo.Result;
import com.jit.buildingsales.vo.ResultList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;


@Api(description = "房源收藏信息")
@RestController
@RequestMapping("/advise")
public class AdviseController extends BasicController {

    @Autowired
    AdviseService adviseService;


    @ApiOperation(value = "提交建议", notes = "提交建议")
    @RequestMapping(value = "/insertAdvise", method = RequestMethod.POST)
    public Result insertAdvise(@RequestBody AdviseVO adviseVO) {


        if (adviseVO.getUserid() <= 0 || StringUtil.isEmpty(adviseVO.getContent())) {
            return new Result().withError("输入不正确");
        }

        try {

            int rows = adviseService.insertAdvise(adviseVO);

            if (rows <= 0) {
                return new Result<>().withError("提交失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }
        return new Result<>();

    }

    @ApiOperation(value = "删除建议", notes = "删除建议")
    @RequestMapping(value = "/deleteAdvise", method = RequestMethod.POST)
    public Result deleteAdvise(@RequestBody AdviseVO adviseVO) {


        if (adviseVO.getId() <= 0 ) {
            return new Result().withError("输入不正确");
        }

        try {

            int rows = adviseService.deleteAdvise(adviseVO);

            if (rows <= 0) {
                return new Result<>().withError("删除失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }
        return new Result<>();

    }


    @ApiOperation(value = "查看建议", notes = "查看建议")
    @RequestMapping(value = "/getAdviseList", method = RequestMethod.POST)
    public ResultList<AdviseDO> getAdviseList(@RequestBody AdviseVO adviseVO) {

        try {

            List<AdviseDO> rows = adviseService.getAdviseList(adviseVO);

            return new ResultList<AdviseDO>().withModel(rows);


        } catch (Exception e) {
            e.printStackTrace();
            return new ResultList().withError("系统错误");
        }

    }


}
