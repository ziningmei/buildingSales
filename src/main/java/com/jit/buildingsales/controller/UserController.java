package com.jit.buildingsales.controller;

import com.jit.buildingsales.global.GlobalConstant;
import com.jit.buildingsales.model.UserDO;
import com.jit.buildingsales.service.UserService;
import com.jit.buildingsales.util.MD5Util;
import com.jit.buildingsales.vo.Result;
import com.jit.buildingsales.vo.ResultList;
import com.jit.buildingsales.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

@Api(description = "用户信息")
@RestController
@RequestMapping("/user")
public class UserController extends BasicController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "登陆", notes = "登陆")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result login(@RequestBody UserVO userVO) {
        try {

            if (StringUtils.isEmpty(userVO.getUsername()) || StringUtil.isEmpty(userVO.getPassword()) || userVO.getRole() <= 0) {
                return new Result().withError("输入不正确");
            }

            userVO.setPassword(MD5Util.md5Encrypt32Lower(userVO.getPassword()));

            UserDO userDO = userService.login(userVO);

            if (userDO != null) {
                httpSession.setAttribute(GlobalConstant.USER, userDO);
                return new Result();
            } else {
                return new Result().withError("账号密码不正确");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

    }

    // 注册处理
    @ApiOperation(value = "注册", notes = "注册")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Result register(@RequestBody UserVO userVO) {


        if (StringUtils.isEmpty(userVO.getUsername()) || StringUtil.isEmpty(userVO.getPassword())) {
            return new Result().withError("输入不正确");
        }

        try {
            userVO.setPassword(MD5Util.md5Encrypt32Lower(userVO.getPassword()));
            int rows = userService.register(userVO);
            if (rows <= 0) {
                return new Result().withError("注册失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

        return new Result();
    }

    @ApiOperation(value = "注销", notes = "注销")
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseBody
    public Result logout() {
        httpSession.removeAttribute(GlobalConstant.USER);
        return new Result<>();
    }


    @ApiOperation(value = "修改个人资料", notes = "修改个人资料")
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public Result updateUser(@RequestBody UserVO user) {

        if (user.getId() <= 0) {
            return new Result().withError("id输入不正确");
        }

        try {
            int rows = userService.updateUser(user);
            if (rows <= 0) {
                return new Result().withError("注册失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }
        return new Result();

    }

    @ApiOperation(value = "获取个人信息", notes = "获取个人信息")
    @RequestMapping(value = "/getUserInfo", method = RequestMethod.POST)
    public Result<UserDO> getUserInfo(@RequestBody UserVO user) {

        if (user.getId() <= 0) {
            return new Result().withError("id输入不正确");
        }

        try {
            UserDO userDO = userService.getUserInfo(user);

            return new Result().withModel(userDO);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

    }

    @ApiOperation(value = "判断是否是管理员", notes = "判断是否是管理员")
    @RequestMapping(value = "/checkAdmin", method = RequestMethod.POST)
    public Result<Boolean> checkAdmin(@RequestBody UserVO user) {

        if (user.getId() <= 0) {
            return new Result().withError("id输入不正确");
        }

        try {
            UserDO userDO = userService.getUserInfo(user);

            if (userDO.getRole() == 2) {
                return new Result().withModel(true);
            } else {
                return new Result().withModel(false);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

    }

    @ApiOperation(value = "删除个人信息", notes = "删除个人信息")
    @RequestMapping(value = "/deleteUserInfo", method = RequestMethod.POST)
    public Result<Boolean> deleteUserInfo(@RequestBody UserVO user) {

        if (user.getId() <= 0) {
            return new Result().withError("id输入不正确");
        }

        try {
            int rows = userService.deleteUserInfo(user);

            if (rows <= 0) {
                return new Result().withError("删除失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Result().withError("系统错误");
        }

        return new Result<>().withModel(true);

    }

    @ApiOperation(value = "查看用户信息列表", notes = "查看用户信息列表")
    @RequestMapping(value = "/getUserList", method = RequestMethod.POST)
    public ResultList<UserDO> getUserList(@RequestBody UserVO userVO) {

        try {

            List<UserDO> rows = userService.getUserList(userVO);

            return new ResultList<UserDO>().withModel(rows);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResultList().withError("系统错误");
        }

    }
}
