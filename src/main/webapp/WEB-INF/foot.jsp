<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
	<style>
		body,h3{
			font-family: "microsoft yahei";
		}
	</style>
</head>
<body>
<div class="footer">
	<div class="container">
		<div class="row-fluid">
			<div class="span4">
                <!-- About -->
		        <div class="headline"><h3>我们的理念</h3></div>	
				<p class="margin-bottom-25">租房、买房、卖房是天下最幸福而又最辛苦的事情。爱屋吉屋用全新的理念和技术，让您不再辛苦，只有幸福</p>	
					
			</div><!--/span4-->	
			
			<div class="span4">
                <div class="headline"><h3>公司简介</h3></div>	
				<p>爱屋之人于2011年3月成立，是全国第一家线上线下整合的专业房地产中介公司。一年来，我们已在北、上、广、深全面铺开，为业主、客户提供最专业的二手房、租房中介服务。
</p>
<p>您只需使用网站或APP，就有爱屋之人经纪人一对一陪您全城看房。只居间，不加价，陪到签约。</p>
			</div><!--/span4-->

			<div class="span4">
	            <!-- Monthly Newsletter -->
		        <div class="headline"><h3>联系我们</h3></div>	
                <address>
					浙江鼎盛房产公司 <br />
					浙江守护大道22号 <br />
					电话: 800 123 3456 <br />
					传真: 800 123 3456 <br />
					邮箱: <a href="mailto:info@anybiz.com" class="">info@anybiz.com</a>
                </address>

               
			</div><!--/span4-->
		</div><!--/row-fluid-->	
	</div><!--/container-->	
</div><!--/footer-->	
<!--=== End Footer ===-->

<!--=== Copyright ===-->
<div class="copyright">
	<div class="container">
		<div class="row-fluid">
			<div class="span8">						
	            <p>沪ICP备14020180&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;增值电信业务经营许可证：沪B2-20160026 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;沪公网安备 31010702001120号</p>
			</div>
			<%-- <div class="span4">	
				<a href="index.html"><img id="logo-footer" src="${path}/assets/img/logo2-default.png" class="pull-right" alt="" /></a>
			</div> --%>
		</div><!--/row-fluid-->
	</div><!--/container-->	
</div><!--/copyright-->	
<!--=== End Copyright ===-->

<script type="text/javascript" src="${path}/assets/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="${path}/assets/js/modernizr.custom.js"></script>
<script type="text/javascript" src="${path}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="${path}/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="${path}/assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="${path}/assets/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="${path}/assets/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="${path}/assets/js/app.js"></script>
<script type="text/javascript" src="${path}/assets/js/pages/index.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
      	App.init();
        App.initSliders();
        Index.initRevolutionSlider();     
        
    });
</script>
<!--[if lt IE 9]>
	<script src="${path}/assets/js/respond.js"></script>
<![endif]-->
</body>
</html>	
