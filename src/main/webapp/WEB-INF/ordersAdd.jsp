<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/assets/js/moment.min.js"></script>
    <script src="${path}/back/js/bootstrap.js"></script>
</head>
<body>
<link rel="stylesheet" type="text/css" href="${path}/assets/css/pikaday.css"/>
<script type="text/javascript" src="${path}/assets/js/pikaday.min.js"></script>
<link href="https://cdn.bootcss.com/pikaday/1.8.0/css/pikaday.min.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/pikaday/1.8.0/pikaday.min.js"></script>
<jsp:include page="head.jsp"></jsp:include>
<br><br>
<div class="container">
    <div class="row-fluid">
        <form class="log-page" action="${path}/ordersShow"/>
        <h3>预约看房</h3>
        <h5>1.请填写 看房时间和备选时间<br>
            2.我们会及时和您取得联系<br>
            3.联系好房东后会及时联系您！<br></h5>

        <div class="input-prepend">
            <span class="add-on"><i class="icon-calendar"></i></span>
            <input id="datepicker" class="input-xlarge" type="text" placeholder="预约时间"/>
        </div>
        <div class="input-prepend">
            <span class="add-on"><i class="icon-calendar"></i></span>
            <input id="datepicker1" class="input-xlarge" type="text" placeholder="备选时间"/>
        </div>
        <div class="controls form-inline">
            <a id="appo" class="btn-u pull-right">预约</a>
        </div>
        <br>


        </form>
    </div><!--/row-fluid-->
</div>
<script type="text/javascript">
    var i18n = { // 本地化
        previousMonth: '上个月',
        nextMonth: '下个月',
        months: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        weekdays: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
        weekdaysShort: ['日', '一', '二', '三', '四', '五', '六']
    }
    var picker = new Pikaday(
        {
            field: document.getElementById('datepicker'),
            firstDay: 1,
            i18n: i18n,
            //format: ' MM月D日',//这里面是改变日期显示的格式的，YYYY年MM月DD日 ,想要什么格式的就在这里面改
            minDate: new Date('2010-01-01'),
            maxDate: new Date('2020-12-31'),
            yearRange: [2000, 2020]
        });

    var picker1 = new Pikaday(
        {
            field: document.getElementById('datepicker1'),
            firstDay: 1,
            i18n: i18n,
            minDate: new Date('2010-01-01'),
            maxDate: new Date('2020-12-31'),
            yearRange: [2000, 2020]
        });
</script>
<jsp:include page="foot.jsp"></jsp:include>
<script>
    $(function () {
        //alert("jj");
        //获取房源拥有人的id
        var admin;
        var house = {};
        house.id =${param.id};
        commonAjax.ajaxPost("${path}/houseResource/getResourceById", house, function (data) {
            //  alert("skjdfh");
            if (data.success) {
                admin = data.model.userid;
                //alert("skjdfh");
                //console.log(data.model);

            }
            else {
                alert(data.errorMsg);
            }
        })
        $("#appo").click(function () {
            var obj = {};
            obj.consumer =${user.id};
            obj.resourceid =${param.id};
            obj.starttime = $("#datepicker").val();
            obj.endtime = $("#datepicker1").val();
            obj.admin = admin;
            obj.status=0;
            commonAjax.ajaxPost("${path}/appointment/appointmentTime", obj, function (data) {
                //  alert("skjdfh");
                if (data.success) {
                    //console.log(data.model);
                }
                else {
                    alert(data.errorMsg);
                }
            })

        })

    })

</script>
</body>
</html>	
