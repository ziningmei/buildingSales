<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>

	<style>	
		p{line-height:30px !important;}
	</style>
</head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>
<div class="container">		
	<div class="row-fluid blog-page blog-item">
        <!-- Left Sidebar -->
    	<div class="span9">
        	<div class="blog margin-bottom-30">
            	<h3>传统商户营销如何移动起来？百度的生态布局开始了</h3>
            	<ul class="unstyled inline blog-info">
                	<li><i class="icon-calendar"></i> 时间：2015-01-02</li>
                	<li><i class="icon-pencil"></i>转载自：</li>
                	<li><i class="icon-comments"></i> <a href="#">编辑人：小王</a></li>
                </ul>
            	
                <div class="blog-img"><img src="${path}/assets/img/posts/1.jpg" alt="" /></div>
                <p>
                1、三房将可能变为刚需<br>在现在的房产市场里<a href="http://jinan.anjuke/ask/SE38082?from=qa_ask_best" target="_blank" rel="nofollow">*</a>两房的刚需属性已经得到肯定*对部分购房者来说，三房也属于刚需范畴*但是如果生了二胎*二房肯定是不够住的*孩子小时候尚且可以用上下床过渡*但如果孩子长大，哪怕性别相同，还是需要一定的个人空间*那个时候：父母一间，孩子一人一间，三房才是刚需产品*<br>2、四房成为改需<br>三房变成了刚需，那么相应的，四房将会变成改需产品*多出来的房间可以作为书房、健身室、对大多数两孩家庭来说<a href="http://jinan.anjuke/ask/SE69C80?from=qa_ask_best" target="_blank" rel="nofollow">最</a>主要的，还可以作为儿童娱乐室，保证儿童的健康发展*<br>3、保姆房会列入开发范畴<br>二孩家庭，只靠父母两个孩子照顾是非常辛苦的*以后的两孩家庭请保姆可能会成为常态*目前市面上就有许多高端产品将保姆房纳入设计范围，相信全面放开二孩政策以后这个范围会更广*<br>
  
                </p>
                

               </div><!--/blog-->

			<hr />

            <!-- Media -->
            
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="${path}/assets/img/sliders/elastislide/9.jpg" alt="" />
                </a>
                <div class="media-body">
                    <h4 class="media-heading">破碎之手 <span>2015-12-01</span></h4>
                    <p>而且也将花费父母大量精力*如果孩子能入读较好的学校，将会大大减轻父母压力*所以</p>
                </div>
            </div>
<hr />
            
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="${path}/assets/img/sliders/elastislide/9.jpg" alt="" />
                </a>
                <div class="media-body">
                    <h4 class="media-heading">破碎之手 <span>2015-12-01</span></h4>
                    <p>而且也将花费父母大量精力*如果孩子能入读较好的学校，将会大大减轻父母压力*所以</p>
                </div>
            </div>
<hr />

            <!-- Leave a Comment -->
            <div class="post-comment">
            	<h3 class="color-green">发表评论</h3>
                <form />
                    <label>姓名</label>
                    <input type="text" class="span7" />
                    <label>邮箱<span class="color-red">*</span></label>
                    <input type="text" class="span7" />
                    <label>信息</label>
                    <textarea rows="8" class="span10"></textarea>
                    <p><button type="submit" class="btn-u">发布</button></p>
                </form>
            </div><!--/post-comment-->
        </div><!--/span9-->

        <!-- Right Sidebar -->
    	<div class="span3">
        	<!-- Photo Stream -->
        	<div class="headline"><h3>搜索新闻</h3></div>
            <div class="input-append  margin-bottom-20">
                <input type="text" class="span9" />
                <button type="button" class="btn-u">搜一下</button>
            </div>
            
            <!-- Blog Tags -->
        	<div class="headline"><h3>关键字</h3></div>
            <ul class="unstyled inline blog-tags">
            	<li><a href="#"><i class="icon-tags"></i> 房产动态</a></li>
            	<li><a href="#"><i class="icon-tags"></i> 国家信息</a></li>
            	<li><a href="#"><i class="icon-tags"></i> 业内新闻</a></li>
            	<li><a href="#"><i class="icon-tags"></i> 市场走向</a></li>
            	<li><a href="#"><i class="icon-tags"></i> 常见问题</a></li>
            	<li><a href="#"><i class="icon-tags"></i> 卖房须知</a></li>
            	<li><a href="#"><i class="icon-tags"></i> 买房须知</a></li>
            	<li><a href="#"><i class="icon-tags"></i> 政策新规</a></li>
            </ul>
        	<!-- Our Services -->
             <div class="posts margin-bottom-20">
                <div class="headline"><h3>精品房源推荐</h3></div>
                <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p> 
                    </dd>
                </dl>
                 <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/1.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p> 
                    </dd>
                </dl>
                
                 <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/2.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p> 
                    </dd>
                </dl>
            </div>
        	

        	
        </div><!--/span3-->
    </div><!--/row-fluid-->        
</div><!--/container-->		
<jsp:include page="foot.jsp"></jsp:include>
</body>
</html>	
