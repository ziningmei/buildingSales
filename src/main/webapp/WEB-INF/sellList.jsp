<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
      <script type="text/javascript"></script>
      <script src="${path}/assets/js/jquery-1.10.2.js"></script>
      <script src="${path}/assets/js/commonAjax.js"></script>
      <script src="${path}/back/js/bootstrap.js"></script>
      <script src="${path}/assets/js/vue.min.js"></script>
  </head>

  <body>
   <jsp:include page="head.jsp"/>

   <div class="container">
    <div class="row-fluid margin-bottom-20">
            <div class="headline"><h3>选择</h3></div>

                <div class="input-group">
                    <input type="text" class="form-control" placeholder="请输入内容：">
                    <span class="icon-search"></span>


        </div>
             <%--<div class="alert alert-info">--%>
                <%--<button type="button" class="close" data-dismiss="alert">&times;</button>--%>
                <%--<strong>您的选择：</strong> 空--%>
            <%--</div>--%>
            <%----%>
            <%--<div class="alert">--%>
                <%--<button type="button" class="close" data-dismiss="alert">&times;</button>--%>
                <%--<strong>户型：</strong>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">一室</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">二室</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">三室</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">四室</a>--%>
            <%--</div>--%>
            <%--<div class="alert">--%>
                <%--<button type="button" class="close" data-dismiss="alert">&times;</button>--%>
                <%--<strong>价格：</strong>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">6千-8千</a>--%>
            <%--</div>                --%>
            <%--<div class="alert">--%>
                <%--<button type="button" class="close" data-dismiss="alert">&times;</button>--%>
                <%--<strong>区域：</strong>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">历下区</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">市中区</a>--%>
            <%--</div>                --%>

        </div>
    <!-- Search Result -->
   <div class="row-fluid">
    	<div id="span9" class="span9">
        	<div class="headline"><h3>为您找到以下房源</h3></div>

                <div id="houstdiv" class="clients-page margin-bottom-20" v-for="item in houselist">
                    <a class="img-hover" :href="'${path}/sellDetail?id='+item.id">
                       <%-- <img :src="'${path}/assets/fangwu/'+item.id+'.png'" alt="" />--%><img :src="'${path}/assets/fangwu/'+item.id+'.png'" alt="" />

                    </a>
                    <p> <a :href="'${path}/sellDetail?id='+item.id" style="font-weight:bold; font-size: 16px;text-decoration:none;">[链家房源、如您所见]{{item.district}}</a>
                        <a href="#" style="float:right; margin-right:50px; font-size: 25px;text-decoration:none; color: red">55万</a>
                    </p>

                    <font style="font-size: 15px; color: gray">
                         {{item.name}}&nbsp;&nbsp;|&nbsp;&nbsp;
                        {{item.area}}平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                            {{item.price}}元/m²&nbsp;&nbsp;|&nbsp;&nbsp;
                        {{item.floor}}层&nbsp;&nbsp;|&nbsp;&nbsp;
                        1996年建造
                        <br>
                       {{item.name}}&nbsp;&nbsp;|&nbsp;&nbsp;
                        {{item.address}}

                        <br>
                        赵经理
                    </font>
                </div>

           <%-- <div class="clients-page margin-bottom-20">
            	<a class="img-hover" href="#">
                	<img src="${path}/assets/fangwu/fangwu1.jpg" alt="" />

				</a>
				<p> <a href="${path}/sellDetail" style="font-weight:bold; font-size: 16px;text-decoration:none;">[链家房源、如您所见]两室朝阳 南北通透 带地下室 可贷款</a>
					<a href="#" style="float:right; margin-right:50px; font-size: 25px;text-decoration:none; color: red">55万</a>
				</p>
            	<font style="font-size: 15px; color: gray">
                     	72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp;
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp;
                       3/6层&nbsp;&nbsp;|&nbsp;&nbsp;
                       1996年建造
                 		<br>
                     	名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;
                        历下区浆水泉路

                        <br>
                   		赵经理
                </font>
           </div>


           <div class="clients-page margin-bottom-20">
            	<a class="img-hover" href="#">
                	<img src="${path}/assets/fangwu/fangwu1.jpg" alt="" />

				</a>
				<p> <a href="#" style="font-weight:bold; font-size: 16px;text-decoration:none;">[链家房源、如您所见]两室朝阳 南北通透 带地下室 可贷款</a>
					<a href="#" style="float:right; margin-right:50px; font-size: 25px;text-decoration:none; color: red">55万</a>
				</p>
            	<font style="font-size: 15px; color: gray">
                     	72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp;
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp;
                       3/6层&nbsp;&nbsp;|&nbsp;&nbsp;
                       1996年建造
                 		<br>
                     	名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;
                        历下区浆水泉路&nbsp;&nbsp;|&nbsp;&nbsp;学区房

                        <br>
                   		赵经理
                </font>
           </div>--%>

        </div><!--/span9-->

    	<div class="span3">
        	<!-- Our Services -->
             <div class="posts margin-bottom-20">
                <div class="headline"><h3>精品房源推荐</h3></div>
                <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp;
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
                    </dd>
                </dl>
                 <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp;
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
                    </dd>
                </dl>

                 <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp;
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
                    </dd>
                </dl>
            </div>


        </div><!--/span3-->
    </div>

       <div class="pagination pagination-large pagination-centered">
                <ul>
                    <li class="disabled"><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li class="active"><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
    <!-- //End Search Result -->
</div><!--/container-->
    <jsp:include page="foot.jsp" flush="false"/>
  <script>
      $(function(){
              //alert("jj");
              var obj={};
              obj.status=1;
         // alert("dd");
             obj.userid=${user.id}
              commonAjax.ajaxPost("${path}/houseResource/getResourceList", obj, function (data) {
                //  alert("skjdfh");
                  if (data.success) {
                      console.log(data.model);
                      var v=new Vue({
                          el:"#span9",
                          data:{
                              houselist:data.model
                          }
                      })
                      // $.each(data.model,function (index,value) {
                      //   //houselist=JSON.parse(value);
                      //     housetmpl
                      // })
                  }
                  else {
                      alert(data.errorMsg);
                  }
              })

          })

  </script>
  </body>
</html>
