<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/back/js/jquery.js"></script>
    <script src="${path}/back/js/bootstrap.min.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/assets/js/vue.min.js"></script>
    <script src="${path}/assets/js/vue-resource-1.3.4.js"></script>
</head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>
<div class="container">

    <!-- Search Result -->
    <div id="tbhouse" class="row-fluid">
        <!--房主看见后通过预约-->
        <p>预约我的房源</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th></th>
                <th>房屋名</th>
                <th>预约者</th>
                <th>预约时间</th>
                <th>备选时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

            <tr v-for="(item,index) in adminlist">
                <td><i class="icon-user"></i></td>
                <td><a href="#">{{housename[index]}}</a></td>
                <td>{{username[index]}}</td>
                <td>2016-01-01</td>
                <td>2016-02-02</td>
                <td><a id="approve" href="#" class="label label-important" @click="approve(item.id)">同意</a><span
                        id="front" class="label label-success">/</span><a class="label label-important"
                                                                          @click="del(item.id)">拒绝</a></td>
            </tr>
            </tbody>
        </table>
        <br/>
        <!--房主看见后通过预约end-->
        <hr/>
        <br/>
        <!--用户自己预约的看房
        <p>我的预约看房</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th></th>
                <th>房屋名</th>
                <th>房主名</th>
                <th>预约时间</th>
                <th>备选时间</th>
                <th>状态</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><i class="icon-user"></i></td>
                <td><a href="#">锦绣泉城2室一厅</a></td>
                <td>锦绣泉城</td>
                <td>2016-01-01</td>
                <td>2016-02-02</td>
                <td>王经理</td>

            </tr>
            </tbody>
        </table>
        <!--用户自己预约的看房-->
    </div>

    <div class="pagination pagination-large pagination-centered">
        <ul>
            <li class="disabled"><a href="#">Prev</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li class="active"><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">Next</a></li>
        </ul>
    </div>
    <!-- //End Search Result -->
</div><!--/container-->
<jsp:include page="foot.jsp"></jsp:include>
<script>
    var v = new Vue({
            el: "#tbhouse",
            data: {
                adminlist: [],
                userlist: [],
                username:[],
                housename:[]
            },
            created:function(){
                //this.getAdmin(${param.id});
                this.getadminList();
                //this.getuserList();

            },
        methods:{
                //根据用户id获取用户名
            getusername:function(id){
                var url="${path}/user/getUserInfo";
                var obj={};
                obj.id=id;
                this.$http.post(url, obj, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                }, {emulateJSON: true}).then(function (response) {
                    if (response.body.success) {
                        //console.log(response.body.model.username);
                        this.username.push(response.body.model.username);

                    } else {
                        alert(response.body.errorMsg);

                    }
                })
            },
            gethousename:function (id) {//根据房源id获取房屋的名字==
                var url = "${path}/houseResource/getResourceById";
                var obj = {};
                obj.id = id;
                this.$http.post(url, obj, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                }, {emulateJSON: true}).then(function (response) {
                    if (response.body.success) {
                        //console.log(response.body.model);
                        this.housename.push(response.body.model.name);
                    } else {
                        alert(response.body.errorMsg);
                    }
                })
            },
        getadminList: function () {
            var url = "${path}/appointment/getMyAppointment";
            var obj = {};
            obj.admin=${user.id};
            this.$http.post(url,obj,{headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }},{emulateJSON:true}).then(function (response) {
                if (response.body.success) {
                    //console.log(response.body.model);
                    this.adminlist=response.body.model;
                    for(var i=0;i<this.adminlist.length;i++){
                        //console.log(this.adminlist[i].consumer);
                        this.getusername(this.adminlist[i].consumer);
                        this.gethousename(this.adminlist[i].resourceid);
                    }
                } else {
                    alert(response.body.errorMsg);
                }
            })
        }

    }
    })
</script>
</body>
</html>	
