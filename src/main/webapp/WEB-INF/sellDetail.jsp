<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html>
<html>
<head>
    <style>
        h2 {
            font-family: "microsoft yahei" !important;
        }
    </style>
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/back/js/bootstrap.js"></script>
    <script src="${path}/assets/js/vue.min.js"></script>
    <script src="${path}/assets/js/vue-resource-1.3.4.js"></script>
</head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>


<div id="housedetail" class="container portfolio-item">
    <div class="row-fluid margin-bottom-20">
        <!-- Carousel -->
        <div class="span7">
            <div id="myCarousel" class="carousel slide">
                <div class="carousel-inner">
                    <div class="item">
                        <img src="${path}/assets/img/carousel/12.png" alt=""/>

                    </div>
                    <div class="item">
                        <img src="${path}/assets/img/carousel/11.png" alt=""/>

                    </div>
                    <div class="item">
                        <img src="${path}/assets/img/carousel/13.png" alt=""/>

                    </div>
                </div>
                <div class="carousel-arrow">
                    <a data-slide="prev" href="#myCarousel" class="left carousel-control"><i
                            class="icon-angle-left"></i></a>
                    <a data-slide="next" href="#myCarousel" class="right carousel-control"><i
                            class="icon-angle-right"></i></a>
                </div>
            </div>
        </div><!--/span7-->
        <!-- //End Tabs and Carousel -->

        <div class="span5">
            <h3>[链家房源、如您所见]{{list.district}}</h3>
            <ul style="list-style:none; margin-left: -1px; font-size: 14px;font-weight: bold">
                <li><i class="icon-user color-green"></i>售价：{{mul(list.price,list.area)}}万元</li>
                <li><i class="icon-tags color-green"></i>面积：{{list.area}}</li>
                <li><i class="icon-info-sign color-green"></i>单价：{{list.price}}元/m²</li>
                <li><i class="icon-group color-green"></i>楼层：{{list.floor}}</li>

                <li><i class="icon-trophy color-green"></i>小区名：{{list.name}}</li>
                <li><i class="icon-eye-open color-green"></i>位置：{{list.address}}</li>
                <li><i class="icon-eye-open color-green"></i>联系人：许经理 15666014201</li>
            </ul>

            <p>


                <%--  <c:out value="${empty isfav}"></c:out>--%>
                <a id="col" class="btn-u btn-u-large" href="javascript:">收藏</a>


                <a class="btn-u btn-u-large" :href="'${path}/ordersAdd?id='+${param.id}">预约看房</a>
            </p>
        </div>
    </div><!--/row-fluid-->
    <!--map开始-->
<div><a class="btn-u btn-u-large" href="${path}/map">查看地图</a></div>
    <!--map结束-->

    <!-- Recent Works -->

    <div class="row-fluid">
        <blockquote>
            <h2 class="color-green">详情介绍</h2>
            <p><span style="font-family: '微软雅黑';font-size:16px; line-height: 30px;">{{list.description}}</span></p>
        </blockquote>
    </div>
    <!-- //End Recent Works -->
    <div class="headline"><h3>优质房源推荐</h3></div>
    <ul class="thumbnails">
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/img/carousel/2.jpg" alt=""/></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                    两室一厅 &nbsp;&nbsp;|&nbsp;&nbsp;
                    8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/img/carousel/2.jpg" alt=""/></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                    两室一厅 &nbsp;&nbsp;|&nbsp;&nbsp;
                    8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/img/carousel/2.jpg" alt=""/></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                    两室一厅 &nbsp;&nbsp;|&nbsp;&nbsp;
                    8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/img/carousel/2.jpg" alt=""/></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                    两室一厅 &nbsp;&nbsp;|&nbsp;&nbsp;
                    8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/img/carousel/2.jpg" alt=""/></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                    两室一厅 &nbsp;&nbsp;|&nbsp;&nbsp;
                    8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/img/carousel/2.jpg" alt=""/></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                    两室一厅 &nbsp;&nbsp;|&nbsp;&nbsp;
                    8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/img/carousel/2.jpg" alt=""/></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                    两室一厅 &nbsp;&nbsp;|&nbsp;&nbsp;
                    8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
                <div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/img/carousel/2.jpg" alt=""/></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                    两室一厅 &nbsp;&nbsp;|&nbsp;&nbsp;
                    8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
            </div>
        </li>
    </ul>
</div>

<jsp:include page="foot.jsp"></jsp:include>

</body>
<script>
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "H+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
</script>
<script>
    $(function () {
        //alert("jj");
        var obj = {};
        var house = {};
        var updatetime = new Date().Format("yyyy-MM-dd");
        house.date = updatetime;
        //alert(house.date)
        house.userid = obj.userid =${user.id};
        house.resourceid = obj.id =${param.id};
        //alert(obj.resourceid);
        //根据url地址传过来的房源id获取房源信息
        commonAjax.ajaxPost("${path}/houseResource/getResourceById", obj, function (data) {
            // alert("skjdfh");
            if (data.success) {
                console.log(data.model);
                var v = new Vue({
                    el: "#housedetail",
                    data: {
                        list: data.model
                    },
                    methods:{
                        mul:function (price,area) {
                            return (price*area)/10000;
                        }
                    }
                })

            }
            else {
                alert(data.errorMsg);
            }
        }),
            //获取该用户的收藏到的房源信息
            commonAjax.ajaxPost("${path}/collection/getMyCollection", obj, function (data) {
                // alert("skjdfh");
                if (data.success) {
                    //console.log(data.model);
                    $.each(data.model, function (index, value) {
                        //收藏的房源id
                        //console.log(value.resourceid);
                        //当前房源的id
                        //console.log(${param.id});
                        //相等时表示该房源已被收藏
                        if (value.resourceid ===${param.id}) {
                            //alert("kk")
                            $("#col").text("已收藏");


                        }
                    })
                    //alert(data.model.resouceid)
                }
                else {
                    alert(data.errorMsg);
                }
            }),
            $("#col").click(function () {
                commonAjax.ajaxPost("${path}/collection/insertCollection", house, function (data) {
                    alert("skjdfh");
                    if (data.success) {
                        // console.log(data.model);
                    }
                    else {
                        alert(data.errorMsg);
                    }
                })
            })
    })
</script>
</html>	
