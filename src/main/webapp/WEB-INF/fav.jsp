<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head><script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/back/js/bootstrap.js"></script></head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>

<div class="container">		
	<div class="row-fluid">
    	<div id="span9" class="span9">
        	<div class="headline"><h3>您收藏的房源信息</h3></div>

                <div v-for="item in collectionlist" class="clients-page margin-bottom-20">
                    <a class="img-hover" :href="'${path}/sellDetail?id='+item.id">
                        <img src="${path}/assets/fangwu/fangwu1.jpg" alt="" />

                    </a>
                    <p> <a :href="'${path}/sellDetail?id='+item.id" style="font-weight:bold; font-size: 16px;text-decoration:none;">[链家房源、如您所见]两室朝阳 南北通透 带地下室 可贷款</a>

                    </p>
                    <font style="font-size: 12px; color: gray">
                        <b style="font-size: 14px; color: red">55万</b>&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                        两室一厅	 <br>
                        {{item.price}}元/m² &nbsp;&nbsp;|&nbsp;&nbsp;
                        3/6层&nbsp;&nbsp;|

                        名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;
                        历下区浆水泉路

                    </font><br><br>
                    <a id="delcollection" href="javascript:" style="float:right" @click="delCol(item.id)">取消收藏</a>
                    <a href="${path}/ordersAdd" style="float:right">预约看房&nbsp;|&nbsp;</a>

                </div><!--/span9-->


            <div class="span3">
                <!-- Our Services -->
                <div class="posts margin-bottom-20">
                    <div class="headline"><h3>精品房源推荐</h3></div>
                    <dl class="dl-horizontal">
                        <dt><a href="#"><img src="assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                        <dd>
                            <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                                两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp;
                                8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><a href="#"><img src="assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                        <dd>
                            <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                                两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp;
                                8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
                        </dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt><a href="#"><img src="assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                        <dd>
                            <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                                两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp;
                                8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p>
                        </dd>
                    </dl>
                </div>


            </div><!--/span3-->
    </div>

       <div class="pagination pagination-large pagination-centered">
                <ul>
                    <li class="disabled"><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li class="active"><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>    
</div><!--/container-->		

<jsp:include page="foot.jsp"></jsp:include>
    <script src="${path}/assets/js/vue.min.js"></script>
    <script>
        $(function(){
            //删除收藏单信息
            /*   "id": 4, "resourceid": 1,"userid": 7*/
            function delCollection(resourceid) {
                var obj={};
                obj.id=$(".clients-page").attr("idx");
                console.log(obj.id);
                obj.userid=${user.id};
                obj.resourceid=resourceid;
                if(!confirm("确定删除？")){
                    return;
                }
                commonAjax.ajaxPost("${path}/collection/deleteCollection",obj, function (data) {
                    //  alert("skjdfh");
                    if (data.success)
                    {
                        console.log(data.model);
                    }
                    else {
                        alert(data.errorMsg);
                    }
                })
            }
            var ids=[];
            var obj={};
            obj.userid=${user.id};
            //获取收藏单表里的数据 根据userid
            commonAjax.ajaxPost("${path}/collection/getMyCollection", obj, function (data) {
                if (data.success)
                {
                    //console.log(data.model);
                    $.each(data.model,function (index,value) {
                        //console.log(value.resourceid);console.log(value.id);
                        $(".clients-page").attr("idx",value.id);
                        //console.log($(".clients-page").attr("idx"));
                        ids.push(value.resourceid);
                    })
                }
                else {
                    alert(data.errorMsg);
                }
                //查看我的收藏房源，根据ids 房源们的id 查询
                //console.log(ids);
                /** {"field": "string","ids": [0],"status": 1}* */
                //alert("kk")
                var colHouse={};
                colHouse.ids=ids;
                //console.log(colHouse.ids);
                colHouse.field="string";
                colHouse.status=1;
                commonAjax.ajaxPost("${path}/houseResource/getResourceListByIds", colHouse, function (data) {
                    // alert("skjdfh");
                    if (data.success)
                    {
                        //alert("jj")
                        //console.log(data.model);
                        var v=new Vue({
                            el:"#span9",
                            data:{
                                collectionlist:data.model
                            },
                            methods:{
                                delCol:function (id) {
                                    delCollection(id);
                                 }
                            }
                        })
                      /*  $.each(data.model,function (index,value) {
                            //console.log(value);
                        })*/
                    }
                    else {
                        alert(data.errorMsg);
                    }
                })

            })
        })
    </script>
</body>
</html>	
