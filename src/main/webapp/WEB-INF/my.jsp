<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/back/js/bootstrap.js"></script>
</head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>

<div class="container">		
	<div class="row-fluid">
    	<div class="span9">
        	<div class="headline"><h3>您发布的房源</h3></div>
            <c:if test="${sessionScope.user.id}">
            <c:forEach items="${houselist}" var="house">
                <div class="clients-page margin-bottom-20">
                    <a class="img-hover" href="#">
                        <img src="${path}/assets/fangwu/fangwu1.jpg" alt="" />

                    </a>
                    <p> <a href="${path}/sellDetail" style="font-weight:bold; font-size: 16px;text-decoration:none;">[链家房源、如您所见]两室朝阳 南北通透 带地下室 可贷款</a>
                        <a href="#" style="float:right; margin-right:50px; font-size: 25px;text-decoration:none; color: red">55万</a>

                    </p>
                    <p>${house.description}</p>
                    <font style="font-size: 15px; color: gray">
                        72平米 &nbsp;&nbsp;|&nbsp;&nbsp;
                            ${house.description}&nbsp;&nbsp;|&nbsp;&nbsp;
                            ${house.price}元/m²&nbsp;&nbsp;|&nbsp;&nbsp;
                        3/6层&nbsp;&nbsp;|&nbsp;&nbsp;
                        1996年建造
                        <br>
                            ${house.name}&nbsp;&nbsp;|&nbsp;&nbsp;
                            ${house.address}${house.district}${house.area}

                        <br>
                        赵经理
                    </font>
                    <a id="delete" href="#" class="icon-remove">删除</a>
                </div>
            </c:forEach>
            </c:if>
            <div class="clients-page margin-bottom-20">
            	<a class="img-hover" href="#">
                	<img src="${path}/assets/fangwu/fangwu1.jpg" alt="" />
                	
				</a>    
				<p> <a href="#" style="font-weight:bold; font-size: 16px;text-decoration:none;">[链家房源、如您所见]两室朝阳 南北通透 带地下室 可贷款</a>
					
				</p>                    
            	<font style="font-size: 12px; color: gray">
                     	<b style="font-size: 14px; color: red">55万</b>&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	 <br>
                       8000元/m² &nbsp;&nbsp;|&nbsp;&nbsp; 
                       3/6层&nbsp;&nbsp;|
                      
                     	名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp; 
                        历下区浆水泉路
                      
                        
                </font><br><br>
                <a href="#" style="float:right">删除房源</a> <a href="#" style="float:right">更新房源&nbsp;|&nbsp;</a>
           </div>
           <div class="clients-page margin-bottom-20">
            	<a class="img-hover" href="#">
                	<img src="${path}/assets/fangwu/fangwu1.jpg" alt="" />
                	
				</a>    
				<p> <a href="#" style="font-weight:bold; font-size: 16px;text-decoration:none;">[链家房源、如您所见]两室朝阳 南北通透 带地下室 可贷款</a>
					
				</p>                    
            	<font style="font-size: 12px; color: gray">
                     	<b style="font-size: 14px; color: red">55万</b>&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	 <br>
                       8000元/m² &nbsp;&nbsp;|&nbsp;&nbsp; 
                       3/6层&nbsp;&nbsp;|
                      
                     	名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp; 
                        历下区浆水泉路
                      
                        
                </font><br><br>
                <a href="#" style="float:right">删除房源</a> <a href="#" style="float:right">更新房源&nbsp;|&nbsp;</a>
           </div>
          
           
        </div><!--/span9-->

    	<div class="span3">
        	<!-- Our Services -->
             <div class="posts margin-bottom-20">
                <div class="headline"><h3>精品房源推荐</h3></div>
                <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p> 
                    </dd>
                </dl>
                 <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p> 
                    </dd>
                </dl>
                
                 <dl class="dl-horizontal">
                    <dt><a href="#"><img src="${path}/assets/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                    <dd>
                        <p>55万&nbsp;&nbsp;|&nbsp;&nbsp;72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²&nbsp;&nbsp;|&nbsp;&nbsp; 名仕豪庭</p> 
                    </dd>
                </dl>
            </div>

        	
        </div><!--/span3-->
    </div>

       <div class="pagination pagination-large pagination-centered">
                <ul>
                    <li class="disabled"><a href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li class="active"><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>    
</div><!--/container-->		

<jsp:include page="foot.jsp"></jsp:include>
</body>
<script>
    $(function(){
        alert("jj");
        var obj={};
        obj.status=1;
        obj.userid=${user.id};
        commonAjax.ajaxPost("${path}/houseResource/getMyResource", obj, function (data) {
            //  alert("skjdfh");
            if (data.success) {
                console.log(data.model);
            }
            else {
                alert(data.errorMsg);
            }
        })


    })

</script>
</html>	
