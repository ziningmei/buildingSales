<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
<script type="text/javascript"></script>
<script src="${path}/assets/js/jquery-1.10.2.js""></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
</head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>
<div class="container">		
		<div class="row-fluid margin-bottom-10">
        	<form id="form-register" class="reg-page" action="" method="" >
            	<h3>注册一个账户</h3>
                <div class="controls">    
                    <label>账号</label>
                    <input id="username" type="text" class="span12" name="username"/>
                     <!-- <label>邮箱 <span class="color-red">*</span></label>
                      <input type="text" class="span12" name="email"/> -->
                </div>
                <div class="controls">
                    <div class="span6">
                        <label>密码<span class="color-red">*</span></label>
                        <input id="password" type="text" class="span12" name="password"/>
                    </div>
                    <div class="span6">
                        <label>重复密码 <span class="color-red">*</span></label>
                        <input id="confirmpassword" type="text" class="span12" name="confirmpassword"/>
                    </div>
                </div>
                <div class="controls form-inline">
                    <label class="checkbox"><input type="checkbox" />&nbsp; 我已经认真阅读 <a href="">网站使用说明</a></label>

                    <a id="registerbtn" class="btn-u pull-right" href="javascript:">注册</a>
                </div>
                <hr />
				<p>已经有账号点击 <a href="${path}/login" class="color-green">登录</a> 登录系统</p>
            </form>
        </div><!--/row-fluid-->
	</div>
<jsp:include page="foot.jsp"></jsp:include>
<script>
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "H+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
</script>
<script>
$().ready(function(){

	//alert("sdhjf");
    /*var myDate=new Date();
    var month = myDate.getMonth() + 1;
    var day = myDate.getDate();
    month = (month.toString().length == 1) ? ("0" + month) : month;
    day = (day.toString().length == 1) ? ("0" + day) : day;
    var result = myDate.getFullYear() + '-' + month + '-' + day; //当前日期*/
	$("#registerbtn").click(function(){
		//alert("kj");
        var obj={};
        var updatetime=new Date().Format("yyyy-MM-dd");
        obj.updatetime= updatetime;
        alert(obj.updatetime);
        obj.username=$("#username").val();
		var password=$("#password").val();
		var confirmpassword=$("#confirmpassword").val();
        obj.password=password;
        //alert("kj");
         obj.role=1;
		if(password==confirmpassword){
            alert("skjdfh");
            commonAjax.ajaxPost("${path}/user/register", obj, function (data) {
                alert("skjdfh");
                if (data.success) {
                    //window.location.href = "${path}/index";
                    alert("注册成功");
                }
                else {
                    alert(data.errorMsg);
                }
            })
		}else{
			alert("两次密码不一致，请重新输入");
		}
		
		
	}) 
	
	
});
</script>
</body>
</html>	
