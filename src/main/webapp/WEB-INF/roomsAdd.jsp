<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/back/js/bootstrap.js"></script>
</head>
<body> 
<jsp:include page="head.jsp"></jsp:include>
<br><br>
<div class="container">		
	<div class="row-fluid">
        <form class="log-page" action=""/>
            <h3>房屋发布</h3> 
            <h5>请认真填写如下内容：</h5>    
             
            <div class="input-prepend">
                <span class="add-on"><i class="icon-star"></i></span>
                <input id="name" class="input-xlarge" type="text" placeholder="房屋名称" />
            </div>
        <div class="input-prepend">
            <span class="add-on"><i class="icon-star"></i></span>
            <input id="district" class="input-xlarge" type="text" placeholder="房屋优点" />
        </div>
        <div class="input-prepend">
            <span class="add-on"><i class="icon-star"></i></span>
            <input id="address" class="input-xlarge" type="text" placeholder="房屋地址" />
        </div>
            
            <div class="input-prepend">
                <span class="add-on"><i class="icon-star"></i></span>
                <select id="floor" class="input-xlarge"  placeholder="楼层">
                	<option>请选择楼层</option>
                	<option>1</option>
                	<option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                    <option>13</option>
                    <option>14</option>
                    <option>15</option>
                    <option>16</option>
                    <option>17</option>
                    <option>18</option>
                    <option>19</option>
                    <option>20</option>
                </select>
               
            </div>
        <div class="input-prepend">
            <span class="add-on"><i class="icon-star"></i></span>
            <input id="price" class="input-xlarge" type="text" placeholder="价格" />
        </div>
        <div class="input-prepend">
            <span class="add-on"><i class="icon-star"></i></span>
            <input id="area" class="input-xlarge" type="text" placeholder="面积" />
        </div>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-star"></i></span> 
                <textarea id="description" class="input-xlarge" rows="10" cols="20"  placeholder="简介" ></textarea>
            </div>


        <div class="input-prepend">
            <input id="input-b3" name="input-b3[]" type="file" class="file" multiple
                   data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload...">
        </div>
            <div class="controls form-inline">
                <a id="addbtn" class="btn-u pull-right" href="#">添加</a>
            </div>
            <br>
            
           
            
        </form>
    </div><!--/row-fluid-->
</div>
 
<jsp:include page="foot.jsp"></jsp:include>
<%--<link href="https://cdn.bootcss.com/bootstrap-fileinput/4.5.3/css/fileinput.min.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.5.3/js/fileinput.min.js"></script>--%>
<script>
    $(function(){

        $("#addbtn").click(function(){

            //alert("sdhjf");

            var obj={};
            obj.name=$("#name").val();
            obj.floor=$("#floor").val()
            //alert(obj.floor)
            obj.district=$("#district").val();
            obj.price=$("#price").val();
            obj.area=$("#area").val();
            obj.description=$("#description").val();
            obj.image=$("#input-b3").val();
            obj.userid=${user.id}
                obj.status=0;
            obj.address=$("#address").val();
            //alert("sdhjf");
            commonAjax.ajaxPost("${path}/houseResource/uploadResource", obj, function (data) {
                //alert("skjdfh");
                if (data.success) {
                    alert("添加成功");
                    $("#addbtn").unbind();
                    $("#addbtn").click(function(){
                        commonAjax.ajaxPostimg("${path}/houseResource/fileUpload", obj.img, function (data) {
                            //alert("skjdfh");
                            if (data.success) {
                                alert("添加成功");

                            }
                            else {
                                alert(data.errorMsg);
                            }
                    });
                })
                }
                else {
                    alert(data.errorMsg);
                }
            })
            //alert("kj");
            //$("#form-ulogin").submit();

        })

    })

</script>
</body>
</html>	
