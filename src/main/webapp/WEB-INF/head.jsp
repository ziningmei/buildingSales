<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<!--[if IE 7]> <html lang="en" class="ie7"> <![endif]-->  
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Unify | Welcome...</title>

    <!-- Meta -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
	 <link rel="stylesheet" href="${path}/assets/plugins/revolution_slider/css/rs-style.css" media="screen" />
    <link rel="stylesheet" href="${path}/assets/plugins/revolution_slider/rs-plugin/css/settings.css" media="screen" />
    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="${path}/assets/plugins/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${path}/assets/css/style.css" />
    <link rel="stylesheet" href="${path}/assets/css/headers/header2.css" />
    <link rel="stylesheet" href="${path}/assets/plugins/bootstrap/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="${path}/assets/css/style_responsive.css" />
    <link rel="shortcut icon" href="favicon.ico" />        
    <!-- CSS Implementing Plugins -->    
    <link rel="stylesheet" href="${path}/assets/plugins/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="${path}/assets/plugins/flexslider/flexslider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${path}/assets/plugins/bxslider/jquery.bxslider.css" />
    <link rel="stylesheet" href="${path}/assets/plugins/horizontal-parallax/css/horizontal-parallax.css" />
    <!-- CSS Theme -->    
    <link rel="stylesheet" href="${path}/assets/css/themes/default.css" id="style_color" />
    <%--<link rel="stylesheet" href="${path}/assets/css/themes/headers/default.css" id="style_color-header-2" />    --%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head> 
	<style>
	.span9 img{
			border:1px solid lightgrey;
			padding:5px
		}
	</style>
	<script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
<body>
<!--=== Style Switcher ===-->    
<!-- <i class="style-switcher-btn style-switcher-btn-option icon-cogs"></i> -->
<!-- <div class="style-switcher style-switcher-inner">
    <div class="theme-close"><i class="icon-remove"></i></div>
    <div class="theme-heading">Theme Colors</div>
    <ul class="unstyled">
        <li class="theme-default theme-active" data-style="default" data-header="dark"></li>
        <li class="theme-blue" data-style="blue" data-header="dark"></li>
        <li class="theme-orange" data-style="orange" data-header="dark"></li>
        <li class="theme-red" data-style="red" data-header="dark"></li>
        <li class="theme-light" data-style="light" data-header="dark"></li>
    </ul>
</div>/style-switcher -->
<!--=== End Style Switcher ===-->    

<!--=== Top ===-->    
<div class="top">
    <div class="container">			        
        <div class="row-fluid">
            <ul class="loginbar inline">
                <li><a href="mailto:info@anybiz.com"><i class="icon-envelope-alt"></i> info@anybiz.com</a></li>	
                <li><a><i class="icon-phone-sign"></i> 010 4202 2656</a></li>
                <c:choose>
                <c:when test="${empty sessionScope.user.username}">
                    <li><a href="${path}/login"><i class="icon-user"></i>登陆</a></li>
                    <li><a href="${path}/register"><i class="icon-edit"></i>注册</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="javascript:"><i class="icon-user"></i>欢迎您${user.username}!</a></li>

                </c:otherwise>
                </c:choose>
                <li><a href="${path}/loginUser"><i class="icon-user"></i>个人信息</a></li>
                <li><a href="${path}/fav"><i class="icon-star"></i>我的收藏</a></li>
                <li><a href="${path}/my"><i class="icon-home"></i>我的房源</a></li>
                 <li><a href="${path}/roomsAdd"><i class="icon-home"></i>发布房源</a></li>
                <li><a href="${path}/ordersShow"><i class="icon-calendar"></i>看房清单</a></li>
                <li><a id="logout" href="${path}/index"><i class="icon-user"></i>注销</a></li>
            </ul>
        </div>        				
    </div><!--/container-->		
</div><!--/top-->
<!--=== End Top ===-->    

<!--=== Header ===-->
<div class="header">               
    <div class="container"> 
        <!-- Logo -->       
        <div class="logo">                                             
            <a href="index.html"><img id="logo-header" src="${path}/assets/img/logo2-default.png" alt="Logo" /></a>
        </div><!-- /logo -->        
                                    
        <!-- Menu -->       
        <div class="navbar">                                
            <div class="navbar-inner">                                  
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a><!-- /nav-collapse -->                                  
                <div class="nav-collapse collapse">                                     
                    <ul class="nav">
                         <li>
                            <a href="${path}/index" class="dropdown-toggle" >首页</a>
                            <b class="caret-out"></b>                        
                        </li>
                         <li>
                            <a href="${path}/sellList" class="dropdown-toggle" >买房</a>
                            <b class="caret-out"></b>                        
                        </li>
                         <li>
                            <a href="${path}/sellList" class="dropdown-toggle" >租房</a>
                            <b class="caret-out"></b>                        
                        </li>
                        <li>
                            <a href="${path}/sellList" class="dropdown-toggle" >写字楼</a>
                            <b class="caret-out"></b>                        
                        </li>
                        <li >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">购房新闻
                                <b class="caret"></b>                            
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active"><a href="${path}/WEB-INF">新闻</a></li>
                            </ul>
                            <b class="caret-out"></b>                        
                        </li>
                        <li>
                            <a href="${path}/contact" class="dropdown-toggle" >联系我们</a>
                            <b class="caret-out"></b>                        
                        </li>
                                                        
                    </ul>
                    <div class="search-open search-open-inner">
                        <div class="input-append">
                            <form />
                                <input type="text" class="span3" placeholder="Search" />
                                <button type="submit" class="btn-u">Search</button>
                            </form>
                        </div>
                    </div>
                </div><!-- /nav-collapse -->                                
            </div><!-- /navbar-inner -->
        </div><!-- /navbar -->                          
    </div><!-- /container -->               
</div>

<script>
$().ready(function(){
	/* alert("sdhjf");
	
	var username='${sessionScope.user.username}';
	if(username!==null){
		
		console.log(username);
		$("#ulogin").addClass("hidden");
		$("#uregister").addClass("hidden");
		$("#umsg").removeClass("hidden");
	}

	$("#udelete").click(function(){
		
        $("#ulogin").addClass("hidden");
        $("#uregister").addClass("hidden");
        $("#umsg").removeClass("hidden");
	})  */
	 $("#logout").click(function () {
	     var obj={};
         commonAjax.ajaxPost("${path}/user/logout", obj, function (data) {
             //alert("skjdfh");
             if (data.success) {
                //alert("成功退出系统");
             }
             else {
                 alert(data.errorMsg);
             }
         })
     })

});

</script>