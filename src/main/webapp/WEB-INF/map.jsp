<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<style type="text/css">
	body, html,#allmap {width: 100%;height: 100%;overflow: hidden;margin:0;font-family:"微软雅黑";}
	</style>
	<script type="text/javascript" src="http://api.map.baidu.com/getscript?v=2.0&ak=mK55PnxMQ79ff5TAzWNdvQufGSTiy5n1&services=&t=20171014112628"></script>
	<%--src="js/jquery-3.1.0.min.js"--%>
	<script type="text/javascript" ></script>
	<script src="${path}/back/js/jquery.js"></script>
<title>地图展示</title>
</head>
<body>
	<div id="r-result">请输入:<input type="text" id="suggestId" size="20" placeholder="请输入位置信息。。。" style="width:450px;" /></div>
	<div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
	<div id="allmap"></div>
</body>
<script type="text/javascript">
    //我所在位置119.98186101,31.77139674
	// 百度地图API功能

	var map = new BMap.Map("allmap");    // 创建Map实例
	map.centerAndZoom(new BMap.Point(119.98186101, 31.77139674), 11);  // 初始化地图,设置中心点坐标和地图级别
	map.addControl(new BMap.MapTypeControl());   //添加地图类型控件
	map.setCurrentCity("北京");          // 设置地图显示的城市 此项是必须设置的
	map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
	
	function G(id) {
		return document.getElementById(id);
	}
	
	var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
			{"input" : "suggestId"
			,"location" : map
		});

		ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
		var str = "";
			var _value = e.fromitem.value;
			var value = "";
			if (e.fromitem.index > -1) {
				value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
			}    
			str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;
			
			value = "";
			if (e.toitem.index > -1) {
				_value = e.toitem.value;
				value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
			}    
			str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
			G("searchResultPanel").innerHTML = str;
		});

		var myValue;
		ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
		var _value = e.item.value;
			myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
			G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;
			//alert(myValue);
			setPlace();
			// 创建地址解析器实例
			var myGeo = new BMap.Geocoder();
			myGeo.getPoint(myValue, function(point){
				if (point) {
		           //alert("地址坐标为："+point.lng+"--"+point.lat);
				}else{
					alert("您选择地址没有解析到结果!");
				}
			});
			
			//var zuobiao = "http://api.map.baidu.com/geocoder?address=常州市新北区三井大厦&output=json&key=37492c0ee6f924cb5e934fa08c6b1676";
			var zuobiao = "http://api.map.baidu.com/geocoder";
			$.ajax({
				url:"JsonGet",
				//contentType: "application/json",
	   			data:{address:myValue,key:'37492c0ee6f924cb5e934fa08c6b1676'},
				success:function(data){
	   				alert("success:"+data);
	   				//for(var i in data){
	   	 			//	alert(data[i].name+"--"+data[i].code);
	   	 			//}
	   			},error:function(){
	   				alert("error");
	   			}
			});
			//location.href=zuobiao;
			//alert(zuobiao);
		});

		function setPlace(){
			map.clearOverlays();    //清除地图上所有覆盖物
			function myFun(){
				var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
				map.centerAndZoom(pp, 18);
				map.addOverlay(new BMap.Marker(pp));    //添加标注
			}
			var local = new BMap.LocalSearch(map, { //智能搜索
			  onSearchComplete: myFun
			});
			local.search(myValue);
		}
	
</script>
</html>