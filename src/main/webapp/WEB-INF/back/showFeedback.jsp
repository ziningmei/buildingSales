<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
    
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
	  <script src="${path}/assets/js/vue.min.js"></script>
	  <script src="${path}/assets/js/vue-resource-1.3.4.js"></script>
  </head>
    <style>
    	.input1{width:120px; margin-left:20px; float:left}
    	.label1{margin-left:20px; float:left}
    	input{
    		height:25px;
    	}
    </style>
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
      <div class="page-header">
				<h1><small>投诉建议信息查看</small></h1>
			</div>
				<form >
			
						<label for="name" class="label1">姓名</label>
						
							<input type="text" class="input1"  id="name" />
						
			
							<input type="submit"  class="input1"  value="查询" />
						<br>
					
				</form>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>编号</th>
						 <th>姓名</th>
						<!--<th>邮箱</th>
						<th>性别</th>
						<th>昵称</th>
						<th>电话</th> -->
						<th>信息</th>
						<th>日期</th>
						<th>操作</th>
						
					</tr>
				</thead>
				<tbody id="tbfeedback">
			
				<tr class="list-users" v-for="(item,index) in fblist">
					<td>{{item.id}}</td>
					<td>{{username[index]}}</td>
					 <td>{{item.content}}</td>
					 <td>{{item.date}}</td>

					<td><a href="javascript:" class="label label-important" @click="look(index)">查看</a><span class="label label-success">/</span><a class="label label-important" @click="delfb(item.id)">删除</a></td>
				</tr>

				</tbody>
			</table>
			<div class="pagination">
				<ul>
					<li><a href="#">上一页</a></li>
					<li class="active">
						<a href="#">1</a>
					</li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">下一页</a></li>
				</ul>
			</div>
			
      </div>
    </div>

	<script>

	var v=new Vue({
		el:"#tbfeedback",
		data:{
		    fblist:[],
			username:[]
		},
		created:function () {
			this.getfbList();
			//this.getUserInfo(this.fblist.userid);
        },
		methods:{
		    look:function (id) {
		        //alert(id);
				alert(this.fblist[id].content);
            },
            delfb:function (id) {
				var obj={};
				obj.id=id;
				var url="${path}/advise/deleteAdvise";
				if(!confirm("确定删除？")){
				    return;
				}
				this.$http.post(url,obj,{headers:{
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
					}},{emulateJSON:true}).then(function (response) {
					if(response.body.success){
					    alert("成功删除");
					    this.getfbList();
					}else{
					    alert(response.body.errorMsg);
					}
                })
            },
		    getUserInfo:function(userid) {
				var obj={};
                obj.id=userid;
                var url="${path}/user/getUserInfo";
                this.$http.post(url,obj,{headers:{
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
					}},{emulateJSON:true}).then(function (response) {
					if(response.body.success){
                        //console.log(response.body.model.username);
						this.username.push(response.body.model.username);
                        //nsole.log(response.body.model.username);
                    }else{
					    alert(response.body.errorMsg)
					}
                })

			},
		    getfbList:function () {
				var url="${path}/advise/getAdviseList";
				var obj={};
				this.$http.post(url,obj,{headers:{
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
					}},{emulateJSON:true}).then(function (response) {
					    if(response.body.success){
                            //console.log(response.body.model);
                            this.fblist=response.body.model;
                            for(var i=0;i<response.body.model.length;i++){
                                //console.log(response.body.model[i].userid);
                                this.getUserInfo(response.body.model[i].userid);
							}
                        }else{
					        alert(response.body.errorMsg);
						}

                })
            }
		}
	})
	</script>
  </body>
</html>
