<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>

      <link rel="stylesheet" href="${path}/back/css/normalize1.css" />
   <script type="text/javascript"></script>
   <script src="${path}/assets/js/jquery-1.10.2.js"></script>
      <script src="${path}/assets/js/commonAjax.js"></script>
      <style type="text/css">
          a{
              text-decoration: none;
          }
      </style>
  </head>
  
  <body>
    <div class="login">
	<h1>Login</h1>
	<form id="form-alogin" method="" action="">
		<input id="form-adminname" type="text" name="adminname" placeholder="用户名" required="required" />
		<input id="form-password" type="password" name="password" placeholder="密码" required="required" />

        <a id="aloginbtn" class="btn btn-primary btn-block btn-large" href="javascript:">登录</a>
	</form>
</div>
<script>

	$().ready(function() {
		<%--  var admin = '<%=session.getAttribute("adminname")%>'; --%>
        $("#aloginbtn").click(function () {
            var obj={};
            obj.username=$("#form-adminname").val();
            obj.password=$("#form-password").val();
            obj.id=2;obj.role=2
            commonAjax.ajaxPost("${path}/user/checkAdmin", obj, function (data) {
                //alert("skjdfh");
                if (data.success) {
                    commonAjax.ajaxPost("${path}/user/login", obj, function (data) {
                        //alert("skjdfh");
                        if (data.success) {
                            window.location.href = "${path}/back/main";

                        }
                        else {
                            alert(data.errorMsg);
                        }
                    })
                }
                else {
                    alert(data.errorMsg);
                }
            })
        })

	});
</script>
  </body>
</html>
