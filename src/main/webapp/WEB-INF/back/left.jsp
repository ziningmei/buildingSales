<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
 <script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
  </head>
  <script type="text/javascript">
  $(document).ready(function(){
	  
         $("li").click(function(){
        	 $("li.active").attr("class","");
        	 $(this).attr("class","active");
         });
  });
  
  
  </script>
  
  <body>
   <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header"><i class="icon-wrench"></i> 用户管理</li>
			  <li class="active"><a target="rightFrame" href="${path}/back/showUser">查看信息</a></li>

			  <li><a target="rightFrame" href="${path}/back/add">用户添加</a></li>
			  
			  <li class="nav-header"><i class="icon-wrench"></i> 房源信息管理</li>
			  <li><a target="rightFrame" href="${path}/back/showHouses">查看信息</a></li>
			  <%--<li><a target="rightFrame" href="showPic.jsp">查看信息（带图）</a></li>--%>
				
			  <li class="nav-header"><i class="icon-wrench"></i> 反馈意见管理</li>
			  <li><a target="rightFrame" href="${path}/back/showFeedback">查看信息</a></li>

             
            </ul>
          </div>
        </div>
        
      </div>
    </div>
  </body>
</html>