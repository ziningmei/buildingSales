<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
	  <script type="text/javascript"></script>
	  <script src="${path}/assets/js/jquery-1.10.2.js"></script>
	  <script src="${path}/back/js/jquery.js"></script>
	  <script src="${path}/back/js/bootstrap.min.js"></script>
	  <script src="${path}/assets/js/commonAjax.js"></script>
	  <script src="${path}/assets/js/vue.min.js"></script>
	  <script src="${path}/assets/js/vue-resource-1.3.4.js"></script>
  </head>
  
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
      	<div class="page-header">
				<h1><small>用户数据修改</small></h1>
			</div>
			<form id="userform" class="form-horizontal">
				<fieldset>
					<div class="control-group">
						<label class="control-label" for="username">姓名</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="username" v-model="list.username" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="password">密码</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="password" v-model="list.password" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="age">年龄</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="age" v-model="list.age"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="sex">性别</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="sex" v-model="list.sex" />
						</div>
					</div>	
					<div class="control-group">
						<label class="control-label" for="role">角色</label>
						<div class="controls">
							<select id="role">
								<option value="admin" selected>管理员</option>
								<option value="user">用户</option>
							</select>
						</div>
					</div>	
					<div class="control-group">
						<label class="control-label" for="active">xx</label>
						<div class="controls">
							<input type="checkbox" id="active" value="1" checked />
						</div>
					</div>
					<div class="form-actions">
						<a class="btn btn-success btn-large" @click="updateUser">保存修改</a> <a class="btn" href="javascript:">取消</a>
					</div>					
				</fieldset>
			</form>
          
       
      </div>
    </div>
  </body>
</html>
<script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>
<script>
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "H+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
</script>
	<script>
	$(document).ready(function() {
		$('.dropdown-menu li a').hover(
		function() {
			$(this).children('i').addClass('icon-white');
		},
		function() {
			$(this).children('i').removeClass('icon-white');
		});
		
		if($(window).width() > 760)
		{
			$('tr.list-users td div ul').addClass('pull-right');
		}
	});
	</script>
<script>
	var v=new Vue({
		el:"#userform",
		data:{
		    list:{}
		},
		created:function(){
		    var id=${param.id};
		    this.getUserInfo(id);
		},
		methods:{
            updateUser:function(){
                var url="${paht}/user/updateUser";
                var updatetime=new Date().Format("yyyy-MM-dd");
                this.list.updatetime= updatetime;
                var role=($('#role option:selected').val()==="admin")?2:1;
                this.list.role=role;
                this.$http.post(url,this.list,{headers:{
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }},{emulateJSON:true}).then(function (response) {
                    if(response.body.success){
                        alert("修改成功")
                    }else{
                        alert(response.body.errorMsg)
                    }
                })

			},
		    getUserInfo:function (id) {
		        var url="${paht}/user/getUserInfo";
				var obj={};
				obj.id=id;
                //console.log(obj.id);
                this.$http.post(url,obj,{headers:{
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
					}},{emulateJSON:true}).then(function (response) {
					    if(response.body.success){
                            this.list=response.body.model;
						}else{
					        alert(response.body.errorMsg)
						}

                })
            }
		}
	})
</script>