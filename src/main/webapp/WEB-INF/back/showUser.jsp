<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
    
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
	  <script src="${path}/assets/js/vue.min.js"></script>
	  <script src="${path}/assets/js/vue-resource-1.3.4.js"></script>
  </head>
    <style>
    	.input1{width:120px; margin-left:20px; float:left}
    	.label1{margin-left:20px; float:left}
    	input{
    		height:25px;
    	}
    </style>
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
      <div class="page-header">
				<h1><small>用户信息查看</small></h1>
			</div>
				<%--<form >
			
						<label for="name" class="label1">姓名</label>
						
							<input type="text" class="input1"  id="name" />
						
			
							<input type="submit"  class="input1"  value="查询" />
						<br>
					
				</form>--%>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>编号</th>
						<th>姓名</th>
						<th>邮箱/年龄</th>
						<th>性别/角色</th>
						<th>性别</th>
						<th>更新时间</th>
						<th>操作</th>
						
					</tr>
				</thead>
				<tbody id="tbuser">
			
				<tr class="list-users" v-for="(item,index) in userlist">
					<td>{{item.id}}</td>
					<td>{{item.username}}</td>
					<td>{{item.age}}</td>
					<td>{{item.role}}</td>
					<td>{{item.sex}}</td>
					<td>{{item.updatetime}}</td>
					<td><a :href="'${path}/back/updateUser?id='+item.id" class="label label-important">修改</a><span class="label label-success">/</span><a class="label label-important" @click="del(item.id)">删除</a></td>
					
				</tr>

				</tbody>
			</table>
			<div class="pagination">
				<ul>
					<li><a href="#">上一页</a></li>
					<li class="active">
						<a href="#">1</a>
					</li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">下一页</a></li>
				</ul>
			</div>
			
      </div>
    </div>
	<script>
        Date.prototype.Format = function (fmt) { //author: meizz
            var o = {
                "M+": this.getMonth() + 1, //月份
                "d+": this.getDate(), //日
                "H+": this.getHours(), //小时
                "m+": this.getMinutes(), //分
                "s+": this.getSeconds(), //秒
                "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                "S": this.getMilliseconds() //毫秒
            };
            if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        }
</script>
	<script>
	var v=new Vue({
	    el:"#tbuser",
		data: {
            userlist: []
        },
		created:function(){
	        this.getList();
		},
		methods:{
            del:function (id) {
                //alert("kk");
                var url="${path}/user/deleteUserInfo";
                var obj={};
                obj.id=id;
                if(!confirm("确定删除？")){
                    return;
				}
                this.$http.post(url,obj,{headers:{
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }},{emulateJSON:true}).then(function (response) {
                    if(response.body.success){
                        alert("删除成功");
                        //刷新列表
                        this.getList();
                    }
                })
            },
	        getList:function(){
              var url="${path}/user/getUserList";
              var obj={};
              var updatetime=new Date().Format("yyyy-MM-dd");
              obj.updatetime=updatetime;
              this.$http.post(url,obj,{headers:{
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                  }},{emulateJSON:true}).then(function (response) {
                  //console.log(response.body.model);
                  this.userlist=response.body.model;
              })
			}

		}
	})
	
	</script>
  </body>
</html>
