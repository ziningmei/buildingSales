<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
  </head>
  
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
      <div class="page-header">
				<h1>Users <small>All users</small></h1>
			</div>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>ID</th>
						<th width="120">PIC</th>
						<th>E-mail</th>
						<th>Phone</th>
						<th>City</th>
						<th>Role</th>
						<th>Status</th>
						
					</tr>
				</thead>
				<tbody>
			
				<tr class="list-users">
					<td>1</td>
					<td><img src="../${path}/back/img/new.jpg" width="120" height="80" /></td>
					<td>dennisbkim@provider.com</td>
					<td>407-xxx-xxxx</td>
					<td>Orlando, FL</td>
					<td>User</td>
					<td><a class="label label-important">修改</a><span class="label label-success">/</span><a class="label label-important">删除</a></td>
					
				</tr>
				<tr class="list-users">
					<td>9</td>
					<td><img src="../${path}/back/img/new.jpg" width="120" height="80" /></td>
					<td>dennisbkim@provider.com</td>
					<td>407-xxx-xxxx</td>
					<td>Orlando, FL</td>
					<td>User</td>
					<td><a class="label label-important">修改</a><span class="label label-success">/</span><a class="label label-important">删除</a></td>
					
				</tr>
				</tbody>
			</table>
			<div class="pagination">
				<ul>
					<li><a href="#">上一页</a></li>
					<li class="active">
						<a href="#">1</a>
					</li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">下一页</a></li>
				</ul>
			</div>
		
       
      </div>
    </div>
  </body>
</html>
<script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>