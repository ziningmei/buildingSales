<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	  <script type="text/javascript"></script>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
	  <script src="${path}/assets/js/jquery-1.10.2.js"></script>
	  <script src="${path}/assets/js/commonAjax.js"></script>
    <style>
    	input{
    			height:30px;
    		}
    	 body{
    		font-family: "微软雅黑";
    	} 
    </style>
  </head>
  
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
      	<div class="page-header">
				<h1><small>信息添加</small></h1>
			</div>
			<form class="form-horizontal">
				<fieldset>
					<div class="control-group">
						<label class="control-label" for="username">姓名</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="username" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="password">密码</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="password"  />
						</div>
					</div>	
					<div class="control-group">
						<label class="control-label" for="age">年龄</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="age" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="sex">性别</label>
						<div class="controls">
							<input type="radio" class="input-xlarge" id="sex" name="sex" value="1"/>男&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" class="input-xlarge"  name="sex" value="0" />女
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label" for="role">角色</label>
						<div class="controls">
							<select id="role">
								<option value="admin" selected>管理员</option>
								<!-- <option value="mod">录入员</option> -->
								<option value="user">用户</option>
							</select>
						</div>
					</div>	
					
					<div class="form-actions">
						<input id="sub" type="submit" class="btn btn-success btn-large" value="保存" />&nbsp;&nbsp;&nbsp; <a class="btn btn-danger btn-large" href="#">取消</a>
					</div>					
				</fieldset>
			</form>
       
      </div>
    </div>
    <script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>
	<script>
        Date.prototype.Format = function (fmt) { //author: meizz
            var o = {
                "M+": this.getMonth() + 1, //月份
                "d+": this.getDate(), //日
                "H+": this.getHours(), //小时
                "m+": this.getMinutes(), //分
                "s+": this.getSeconds(), //秒
                "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                "S": this.getMilliseconds() //毫秒
            };
            if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        }
	</script>
	<script>
	$(document).ready(function() {
		$('.dropdown-menu li a').hover(
		function() {
			$(this).children('i').addClass('icon-white');
		},
		function() {
			$(this).children('i').removeClass('icon-white');
		});
		
		if($(window).width() > 760)
		{
			$('tr.list-users td div ul').addClass('pull-right');
		}
	});
	</script>
  <script>
	  $("#sub").click(function () {
          var obj={};
          var updatetime=new Date().Format("yyyy-MM-dd");
          obj.updatetime= updatetime;
          var sex= $('input[name="sex"]:checked ').val();
          obj.sex=parseInt(sex);
          //console.log(obj.sex);
          obj.username=$("#username").val();
          var password=$("#password").val();
          obj.password=password;
          obj.age=$("#age").val();
          //alert("kj");
          var role=($('#role option:selected').val()==="admin")?2:1;
          //console.log(role);
          obj.role=role;
          commonAjax.ajaxPost("${path}/user/register",obj,function (data){
              if(data.success){
                  alert("添加成功");
			  }else{
                  alert(data.errorMsg);
			  }
         })
	  })
  </script>
  </body>
</html>
