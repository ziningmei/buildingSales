<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
    
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/assets/js/vue.min.js"></script>
    <script src="${path}/assets/js/vue-resource-1.3.4.js"></script>

  </head>
    <style>
    	.input1{width:120px; margin-left:20px; float:left}
    	.label1{margin-left:20px; float:left}
    	input{
    		height:25px;
    	}
    </style>
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
      <div class="page-header">
				<h1><small>房源信息信息查看</small></h1>
			</div>

			<table id="tbhouse" class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>编号</th>
						<th>房主</th>
						<th>小区名</th>
						<th>地址</th>
						<th>面积</th>
						<th>价钱</th>
						<th>操作</th>
						
					</tr>
				</thead>
				<tbody id="tbuser">
			
				<tr class="list-users" v-for="(item,index) in list">
					<td>{{item.id}}</td>
					<td>{{username[index]}}</td>
					<td>{{item.name}}</td>
					<td>{{item.address}}</td>
					<td>{{item.area}}</td>
					<td>{{item.price}}</td>
					<td>
						<div v-if="status"><a id="approve" href="#" class="label label-important" @click="approve(item.id)">批准</a><span id="front" class="label label-success">/</span></div><a class="label label-important" @click="del(item.id)">删除</a></td>
					
				</tr>
				<%--<tr class="list-users">
					<td>9</td>
					<td>Dennis B. Kim</td>
					<td>dennisbkim@provider.com</td>
					<td>407-xxx-xxxx</td>
					<td>Orlando, FL</td>
					<td>User</td>
					<td><a class="label label-important">修改</a><span class="label label-success">/</span><a class="label label-important">删除</a></td>
					
				</tr>--%>
				</tbody>
			</table>
			<div class="pagination">
				<ul>
					<li><a href="#">上一页</a></li>
					<li class="active">
						<a href="#">1</a>
					</li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">下一页</a></li>
				</ul>
			</div>
			
      </div>
    </div>
	<script>
	$().ready(function(){
        var username=[];
	    function getUserInfo(id) {
			var obj={};
			obj.id=id;
			commonAjax.ajaxPost("${path}/user/getUserInfo",obj,function (data) {
              if(data.success){
                  //console.log(data.model.username);
                 username.push(data.model.username)
			  }else{
                  alert(data.errorMsg)
			  }
            })
        }
	    var obj={};
	    var userid=[];
	    obj.status=0;
		commonAjax.ajaxPost("${path}/houseResource/getResourceList",obj,function (data) {
		    if(data.success){
		        $.each(data.model,function (index,value) {
                    //console.log(value.userid);
                    userid.push(value.userid);
                    //console.log(value.status);
                })
					// console.log(userid);
		        //获取对应id的用户名
				for(var i=0;i<userid.length;i++){
                    getUserInfo(userid[i]);
				}
                //console.log(username);
				//data.model.username=username;
                var v=new Vue({
                    el:"#tbhouse",
                    data:{
                        list:data.model,
						username:username,
						status:false
                    },
					methods:{
                        del:function (id) {
                            //url地址
							var url="${path}/houseResource/deleteResource";
                            var obj={};
                            obj.id=id
							//post
							this.$http.post(url,obj,{headers:{
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
								}},{emulateJSON:true}).then(function (response)                                            {
							    if(response.body.success){
							        alert("成功删除")

								}
                            })
						},
						approve:function (id) {
                            //url地址
                            var url="${path}/houseResource/updateResource";
                            var obj={};
                            obj.id=id;
                            obj.status=1;
                            this.$http.post(url,obj,{headers:{
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                }},{emulateJSON:true}).then(function (response)                                            {
                                if(response.body.success){
                                    alert("kk");
                                    $("#approve").addClass("hide");
                                    $("#front").addClass("hide");
                                    this.status=true;
                                }
                            })
                        }
			}
							/*get请求
							this.$http.get(url)     //发出请求
							.then(function(response){
							response.body   //为返回的数据

							})
							*
							* */
						/*	//post请求
							this.$http.post(url,{"name":"kk"},{emulateJSON:true})
								.then(function (response) {
									response.body;
                                })
                        }*/

            //console.log(data.model);

	})
		    }else{
		        alert(data.errorMsg)
            }
            })
	})
	
	</script>
  </body>
</html>
