<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
    	.input1{width:120px; margin-left:20px; float:left}
    	.label1{margin-left:20px; float:left}
    </style>
  </head>
  
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
      	<div class="page-header">
				<h1> <small>统计图表</small></h1>
			</div>
			<form >
			
						<label for="name" class="label1">姓名</label>
						
							<input type="text" class="input1"  id="name" />
						
						<label  for="name" class="label1">姓名</label>
						
							<input type="text"  class="input1"  id="name" />
							<input type="submit"  class="input1"  value="统计" />
						<br>
					
				</form>
			<div id="placeholder" style="width:80%;height:300px;clear:both"></div>
      </div>
    </div>
  </body>
</html>
 <script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/jquery.flot.js"></script>
	<script src="${path}/back/js/jquery.flot.resize.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>
	<script>
	$(function () {
		
		var data = [
		    		{
		    			label: 'Page Views',
		    			data: [[0, 19000], [1, 15500], [2, 11100], [3, 15500]]
		    		}];
		    	var options = {
		    			legend: {
		    				show: true,
		    				margin: 10,
		    				backgroundOpacity: 0.5
		    			},
		    			points: {
		    				show: true,
		    				radius: 3
		    			},
		    			lines: {
		    				show: true
		    			},
		    			grid: {
		    				borderWidth:1,
		    				hoverable: true
		    			},
		    			xaxis: {
		    				axisLabel: 'Month',
		    				ticks: [[0, 'Jan'], [1, 'Feb'], [2, 'Mar'], [3, 'Apr'], [4, 'May'], [5, 'Jun'], [6, 'Jul'], [7, 'Aug'], [8, 'Sep'], [9, 'Oct'], [10, 'Nov'], [11, 'Dec']],
		    				tickDecimals: 0
		    			},
		    			yaxis: {
		    				tickSize:1000,
		    				tickDecimals: 0
		    			}
		    		};
		var previousPoint = null;
		$("#placeholder, #visits").bind("plothover", function (event, pos, item) {
			if (item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					showTooltip(item.pageX, item.pageY, item.series.label + ": " + item.datapoint[1]);
				}
			}
			else {
				$("#tooltip").remove();
				previousPoint = null;            
			}
		});
		
		$.plot( $("#placeholder") , data, options );
	});
	</script>