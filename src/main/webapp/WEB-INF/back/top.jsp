<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
     <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
	<link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
  </head>
  
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">鼎鼎房坞管理系统后台</a>
          <div class="btn-group pull-right">
			<a class="btn" ><i class="icon-user"></i>欢迎管理员：${sessionScope.user.username}</a>
           	<a class="btn" href="my-profile.html"><i class="icon-user" id="logout"></i> 退出</a>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
<script type="text/javascript"></script>
<script src="${path}/assets/js/jquery-1.10.2.js"></script>
<script src="${path}/assets/js/commonAjax.js"></script>
  <script src="${path}/back/js/jquery.js"></script>
	<script src="${path}/back/js/bootstrap.min.js"></script>

<script>
    $("#logout").click(function () {
        var obj={};
        commonAjax.ajaxPost("${path}/user/logout", obj, function (data) {
            //alert("skjdfh");
            if (data.success) {
                //alert("成功退出系统");
                window.location.href("${path}/back/login");
            }
            else {
                alert(data.errorMsg);
            }
        })
    })
</script>