<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>	
	<style>
		.desc h4{
			font-family:"microsoft yahei"
		}
		
	</style>
<script type="text/javascript"></script>
<script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
</head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<jsp:include page="huandengpian.jsp"></jsp:include>
<div class="container">	
<br><br>
	<div class="headline"><h3>我们的服务</h3></div>
	<div class="row-fluid service-alternative"><!-- remove class "service-alternative" to hide green onmouseover effect-->
    	<div class="span4">
    		<div class="service clearfix">
    			<div class="circle">
    				<i class="icon-resize-small"></i>
    			</div>
    			<div class="desc">
    				<h4>安心买房</h4>
                    <p>1.所有房源真是可靠</p>
                    <p>2.帮您选择优秀房源</p>
                    <p>3.一站式买房服务</p>
                    <p>4.线下拥有500+实体店</p>
    			</div>
    		</div>	
    	</div>
    	<div class="span4">
    		<div class="service clearfix">
    			<div class="circle">
    				<i class="icon-cogs"></i>
    			</div>
    			<div class="desc">
    				<h4>安心卖房</h4>
                    <p>1.简单几步开始卖房</p>
                    <p>2.精准的房屋推广</p>
                    <p>3.更便捷的卖房服务</p>
                    <p>4.省心省力省时</p>	
    			</div>
    		</div>	
    	</div>
    	<div class="span4">
    		<div class="service clearfix">
    			<div class="circle">
    				<i class="icon-plane"></i>
    			</div>
    			<div class="desc">
    				<h4>看房顾问</h4>
                    <p>1.您的私人看房顾问</p>
                    <p>2.帮您挑选精品房屋</p>
                    <p>3.帮您核实房屋信息</p>
                    <p>4.买房、卖房竟如此简单</p>		
    			</div>
    		</div>	
    	</div>			    
	</div>
	<br>
	<div class="row-fluid">
     
     
    	<div class="span9">
        	<div class="headline"><h3>热点追踪</h3></div>
            <div class="clients-page margin-bottom-20">
            	<a class="img-hover" href="#">
                	<img src="${path}/assets/fangwu/fangwu2.jpg" alt="" />
				</a>                        
                <p style="font-size: 17px;font-weight: bold" >大学生可零首付买房？政府你要淡定</p>
                 <p>沈阳的这一房地产新政，因“大学生可零首付买房”广泛传播并引发热议。当晚，沈阳官方发布消息称，“零首付”暂不具备出台条件</p>
            </div>
            <div class="clients-page margin-bottom-20">
            	<a class="img-hover" href="#">
                	<img src="${path}/assets/fangwu/fangwu3.jpg" alt="" />
				</a>                        
               <p style="font-size: 17px;font-weight: bold" >幸福可能会迟到 但不会缺席</p>
                 <p>韩剧都抛弃女主角得了绝症，男主不离不弃的桥段了，你还在幻想腻歪的爱情之旅吗？爱情里价值观之间的碰撞才格外吸引人。今天小编cos一下...</p>
           </div>
            <div class="clients-page margin-bottom-20">
            	<a class="img-hover" href="#">
                	<img src="${path}/assets/fangwu/fangwu4.jpg	" alt="" />
				</a>                        
                <p style="font-size: 17px;font-weight: bold" >【优惠打折】任志强：2016年房价还得涨</p>
                 <p>首付给你降了，利息也给你降了，现在连契税都给你降了，全世界都在帮你买房子，而你还在傻傻等降价吗？再傻等，你买三房的钱可能只够买两房了！</p>
           		<p>付乐|16-02-29</p>	
           </div>
           
        </div><!--/span9-->

    	<div class="span3">
        	<!-- Our Services -->
            <div class="who margin-bottom-30">
                <div class="headline"><h3>房产问答</h3></div>
                <ul  style="list-style:none; margin-left: -1px; line-height:50px; font-size: 12px;font-weight: bold">
                    <li><a href="#"><i class="icon-desktop"></i>房产转让有什么条件？</a></li>
                    <li><a href="#"><i class="icon-bullhorn"></i>经济适用房和廉租房有何分别？</a></li>
                    <li><a href="#"><i class="icon-globe"></i>定金和订金有什么不同？</a></li>
                    <li><a href="#"><i class="icon-group"></i>产权到期后怎么办？</a></li>
                	 <li><a href="#"><i class="icon-home"></i>怎样办理二手房产权变更登记？</a></li>
                    <li><a href="#"><i class="icon-envelope-alt"></i>新房交房有哪些注意事项？</a></li>
                    <li><a href="#"><i class="icon-phone-sign"></i>商业贷款如何转为公积金贷款？</a></li>
                    <li><a href="#"><i class="icon-phone-sign"></i>买房子高层风水好不好？</a></li>
                    <li><a href="#"><i class="icon-phone-sign"></i>贷款期间，如果银行利率调整怎么办？</a></li>
                    <li><a href="#"><i class="icon-phone-sign"></i>法院会不会认可阴阳合同？</a></li>
                    <li><a href="#"><i class="icon-phone-sign"></i>合伙购房协议怎么写，会出现哪些法律问题？</a></li>
                    <li><a href="#"><i class="icon-phone-sign"></i>办商业贷款一般要多少天才能批下来？</a></li>
                    <li><a href="#"><i class="icon-phone-sign"></i>二手房贷款、过户费用的计算？</a></li>
                </ul>
            </div>

        	
        </div><!--/span3-->
    </div>
	 <a href="#"><img src="${path}/assets/img/xx.png" width="100%" /></a>
	 <br> <br>
	<div class="headline"><h3>优质房源推荐</h3></div>
    <ul class="thumbnails">
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
            	<div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/fangwu/fangwu1.jpg" alt="" /></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>					
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²<br> 名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;经十路110号</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
            	<div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/fangwu/fangwu2.jpg" alt="" /></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>					
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²<br> 名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;经十路110号</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
            	<div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/fangwu/fangwu3.jpg" alt="" /></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>					
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²<br> 名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;经十路110号</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
            	<div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/fangwu/fangwu4.jpg" alt="" /></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>					
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²<br> 名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;经十路110号</p>
            </div>
        </li>	
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
            	<div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/fangwu/fangwu5.jpg" alt="" /></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>					
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²<br> 名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;经十路110号</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
            	<div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/fangwu/fangwu3.jpg" alt="" /></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>					
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²<br> 名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;经十路110号</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
            	<div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/fangwu/fangwu1.jpg" alt="" /></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>					
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²<br> 名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;经十路110号</p>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail-style thumbnail-kenburn">
            	<div class="thumbnail-img">
                    <div class="overflow-hidden"><img src="${path}/assets/fangwu/fangwu2.jpg" alt="" /></div>
                    <a class="btn-more hover-effect" href="#">查看详情 +</a>					
                </div>
                <h3><a class="hover-effect" href="#">55万</a></h3>
                <p>72平米 &nbsp;&nbsp;|&nbsp;&nbsp; 
                       两室一厅	&nbsp;&nbsp;|&nbsp;&nbsp; 
                       8000元/m²<br> 名仕豪庭&nbsp;&nbsp;|&nbsp;&nbsp;经十路110号</p>
            </div>
        </li>
    </ul>
	
           
     <br>
      <div id="clients-flexslider" class="flexslider home clients">
        <div class="headline"><h3>友情链接</h3></div>    
        <ul class="slides">
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/hp_grey.png" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/igneus_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/igneus.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/vadafone_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/vadafone.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/walmart_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/walmart.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/shell_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/shell.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/natural_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/natural.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/aztec_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/aztec.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/gamescast_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/gamescast.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/cisco_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/cisco.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/everyday_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/everyday.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/cocacola_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/cocacola.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/spinworkx_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/spinworkx.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/shell_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/shell.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/natural_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/natural.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/gamescast_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/gamescast.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/everyday_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/everyday.png" class="color-img" alt="" />
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="${path}/assets/img/clients/spinworkx_grey.png" alt="" />
                    <img src="${path}/assets/img/clients/spinworkx.png" class="color-img" alt="" />
                </a>
            </li>
        </ul>
    </div>
</div>

<jsp:include page="foot.jsp"></jsp:include>
</script>
</body>
</html>	
