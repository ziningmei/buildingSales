<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();

String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%> 
<!-- ${path}/user/login -->
<!DOCTYPE html>
<html>
<head>
<base <%=basePath %>>
<script type="text/javascript"></script>
<script src="${path}/assets/js/jquery-1.10.2.js"></script>
<script src="${path}/assets/js/commonAjax.js"></script>
</head>

<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>
<div class="container">		
	<div class="row-fluid">
        <form id="form-ulogin" class="log-page" action="" method="" >
            <h3>请登陆</h3>    
            <div class="input-prepend">
                <span class="add-on"><i class="icon-user"></i></span>
                <input id="form-username" name="username" class="input-xlarge" type="text" placeholder="用户名" />
            </div>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-lock"></i></span>
                <input id="form-password"  name="password" class="input-xlarge" type="password" placeholder="密码" />
            </div>
            <div class="controls form-inline">
                <label class="checkbox"><input type="checkbox" /> 保持登录</label>
                <%--<input id="loginbtn" class="btn-u pull-right" type="submit" value="登录"/>--%>
                <a id="loginbtn" class="btn-u pull-right" href="javascript:">登录</a>
            </div>
            <hr />
            <h4>忘记密码 ?</h4>
            <p>别担心, <a class="color-green" href="#">点此</a> 重置密码。</p>
             <font  color="#FF0000">${requestScope.message}</font>
        </form>
       
    </div><!--/row-fluid-->
</div>

<jsp:include page="foot.jsp"></jsp:include>
<script>
$(function(){
	//alert("sdhjf");
	$("#loginbtn").click(function(){
		
			//alert("sdhjf");
		var obj={};
		obj.username=$("#form-username").val();
		obj.password=$("#form-password").val();
		obj.role=1;
		//alert("sdhjf");
		commonAjax.ajaxPost("${path}/user/login", obj, function (data) {
        	//alert("skjdfh");
            if (data.success) {

                window.location.href = "${path}/index";

            }
            else {
                alert(data.errorMsg);
            }
        })
		//alert("kj");
		//$("#form-ulogin").submit();
		
	}) 
	
	
});
</script>

</body>
</html>	
