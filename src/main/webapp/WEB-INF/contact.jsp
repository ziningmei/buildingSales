<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/back/js/bootstrap.js"></script>
</head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>
<div class="container">		
	<div class="row-fluid">
		<div class="span9">
            <div class="headline"><h3>联系我们</h3></div>
            <p>您有任何投诉或者建议，请发信息给我们，我们将定期慎重查阅。</p><br />
			<form>
                <label>标题</label>
                <input type="text" class="span7 border-radius-none" />
                <label>内容</label>
                <textarea rows="8" class="span7" id="content"></textarea>
                <p><button id="sub" type="submit" class="btn-u">提交</button></p>
            </form>
        </div><!--/span9-->
        
		<div class="span3">
        	<!-- Contacts -->
            <div class="headline"><h3>联系我们</h3></div>
            <ul class="unstyled who margin-bottom-20">
                <li><a href="#"><i class="icon-home"></i>浙江守护大道22号 </a></li>
                <li><a href="#"><i class="icon-envelope-alt"></i>fangfangs@163.com</a></li>
                <li><a href="#"><i class="icon-phone-sign"></i>138 5286 1971</a></li>
                <li><a href="#"><i class="icon-globe"></i>http://www.aifang.com</a></li>
            </ul>

        	<!-- Business Hours -->
            <div class="headline"><h3>业务受理时间</h3></div>
            <ul class="unstyled">
            	<li><strong>周一到周五:</strong> 上午10时 -- 晚上20时</li>
            	<li><strong>周六:</strong> 上午11时 -- 下午15时</li>
            	<li><strong>周日:</strong> 休息</li>
            </ul>

        	<!-- Why we are? -->
            <div class="headline"><h3>我们的服务</h3></div>
            <p>主要业务分为资产管理、交易管理和金融管理，具体涉及租赁、新房、二手房、资产管理、海外房产、互联网平台、金融、理财、后房产市场等领域。</p>
            <ul class="unstyled">
            	<li><i class="icon-ok color-green"></i> 专注专业</li>
            	<li><i class="icon-ok color-green"></i> 诚信至上</li>
            	<li><i class="icon-ok color-green"></i> 服务第一</li>
            </ul>
        </div><!--/span3-->
    </div><!--/row-fluid--></div>
<jsp:include page="foot.jsp"></jsp:include>
<script>
    $(function(){
       // alert("jj");
        $("#sub").click(function () {
            var obj={};
            obj.content=$("#content").val();
            obj.userid= ${user.id};
                <%--${sessionScope.user.id}--%>
                commonAjax.ajaxPost("${path}/advise/insertAdvise", obj, function (data) {
                    //  alert("skjdfh");
                    if (data.success) {
                        alert("已发送成功");
                    }
                    else {
                        alert(data.errorMsg);
                    }
                })
        })

    })
</script>
</body>
</html>	
 