<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
    <link href="${path}/back/css/bootstrap.css" rel="stylesheet">
    <link href="${path}/back/css/site.css" rel="stylesheet">
    <link href="${path}/back/css/bootstrap-responsive.css" rel="stylesheet">
    <script type="text/javascript"></script>
    <script src="${path}/assets/js/jquery-1.10.2.js"></script>
    <script src="${path}/back/js/jquery.js"></script>
    <script src="${path}/back/js/bootstrap.min.js"></script>
    <script src="${path}/assets/js/commonAjax.js"></script>
    <script src="${path}/assets/js/vue.min.js"></script>
    <script src="${path}/assets/js/vue-resource-1.3.4.js"></script>
</head>
<body>

<jsp:include page="head.jsp"></jsp:include>
<br><br>
<div class="container">		
		<div class="row-fluid margin-bottom-10">
        	<form class="reg-page" id="userfor"/>
            	<h3>您的个人信息如下</h3>
                <div class="controls">
                   <%--<form  class="form-horizontal">--%>

                            <div class="control-group">
                                <label class="control-label" for="username">用户名</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" id="username" v-model="list.username" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="password">密码</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" id="password" v-model="list.password" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="age">年龄</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" id="age" v-model="list.age"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="sex">性别</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" id="sex" v-model="list.sex" />
                                </div>
                            </div>

                                <a class="btn btn-success" @click="updateUser">保存修改</a> <a class="btn" href="javascript:">取消修改</a>



                </div>
                
                <hr />

            </form>
        </div><!--/row-fluid-->
	</div>
<jsp:include page="foot.jsp"></jsp:include>
<script>
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "H+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
</script>
<script>
    var v=new Vue({
        el:"#userfor",
        data:{
            list:{}
        },
        created:function(){
            var id=${user.id};
            this.getUserInfo(id);
        },
        methods:{
            updateUser:function(){
                var url="${paht}/user/updateUser";
                var updatetime=new Date().Format("yyyy-MM-dd");
                this.list.updatetime= updatetime;
                this.list.role=1;
                this.$http.post(url,this.list,{headers:{
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }},{emulateJSON:true}).then(function (response) {
                    if(response.body.success){
                        alert("修改成功")
                    }else{
                        alert(response.body.errorMsg)
                    }
                })

            },
            getUserInfo:function (id) {
                var url="${paht}/user/getUserInfo";
                var obj={};
                obj.id=id;
                //console.log(obj.id);
                this.$http.post(url,obj,{headers:{
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }},{emulateJSON:true}).then(function (response) {
                    if(response.body.success){
                        console.log(response.body.model);
                        this.list=response.body.model;
                    }else{
                        alert(response.body.errorMsg)
                    }

                })
            }
        }
    })
</script>
</body>
</html>	
