/*
 * commonAjax.js
 * Description：共同ajax调用js
 * Creator：CaoJian
 * CreateDate：2014-03-18
 */
(function($) {
	var commonAjax = function() {
		// 2015年10月22日20:27:53
		var ajaxObj = {
				

			/**
			 * 共同ajax调用 (post) 
			 * 
			 * @param url
			 *            action路径
			 * @param obj
			 *            json格式参数
			 * @param event
			 *            回调函数
			 */
			ajaxPost : function(url, obj, fn) {
                $.ajax({
                            url: url,
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            type: "post",
                            dateType: "application/json",
                            data: JSON.stringify(obj),
                            success:function(result){
                                fn(result);
                    },
                    error:function () {
                        alert("加载出错");
                    },
                })
			},
            ajaxPostimg : function(url, obj, fn) {
                $.ajax({
                    url: url,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    },
                    type: "post",
                    dateType: "application/json",
                    data: JSON.stringify(obj),
                    success:function(result){
                        fn(result);
                    },
                    error:function () {
                        alert("加载出错");
                    },
                })
            },
            ajaxGet : function(url,fn) {
                $.ajax({
                    url: url,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    type: "get",
                    dateType: "application/json",

                    success:function(result){
                        fn(result);
                    },
                    error:function () {
                        alert("加载出错");
                    },
                })
            }
		};
		return ajaxObj;
	}();
	window.commonAjax = commonAjax;
}(jQuery));